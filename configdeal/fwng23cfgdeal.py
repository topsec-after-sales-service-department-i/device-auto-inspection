#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: ngcondeal
# @Function:
# @Author: RyneZ
# @Time: 2021/4/14 11:52
import re
from numpy import *
import time


def fwng23_model(outputDic):
    return outputDic[1].strip()


def fwng23_version(outputDic):
    version = ''
    PSN = ''
    # print("正在获取设备系统版本及序列号")
    for line in outputDic:
        # print(line.strip())
        line = line.strip()
        verReg = re.compile('^(VERSION: )(.*)')
        psnReg = re.compile('^(PSN: )(.*)')
        matchVer = re.match(verReg, line)  # 匹配版本号
        matchPSN = re.match(psnReg, line)  # 匹配序列号
        if matchVer:
            version = matchVer.group(2)
            continue
        if matchPSN:
            PSN = matchPSN.group(2)
            continue
    return version, PSN


def fwng23_time(outputDic):
    line = outputDic[1].strip()
    times = line.split()
    devicetime = times[1] + ' ' + times[2]
    localtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    return devicetime, localtime


def fwng23_ntp(outputDic):
    address = ''
    status = ''
    # print(outputDic)
    for line in outputDic:
        line = line.strip()
        ntpaddressReg = re.compile('^(system ntp set).*address(.*)')
        ntpstatusReg = re.compile('^(system).*(stop|start)')
        matchaddress = re.match(ntpaddressReg, line)
        matchstatus = re.match(ntpstatusReg, line)
        if matchaddress:
            address = matchaddress.group(2)
        if matchstatus:
            status = matchstatus.group(2)
    return address, status


def fwng23_uptime(outputDic):
    # print("正在处理设备运行天数")
    uptime = 0
    for line in outputDic:
        line = line.strip()
        uptimeReg = re.compile('^(UP )(.*)(days.*)')
        matchtime = re.match(uptimeReg, line)  # 匹配系统运行天数
        if matchtime:
            uptime = matchtime.group(2)
    return uptime


def fwng23_sysinfo(outputDic):
    '''
    根据单次获取的cpu值计算平均利用率，通过多次调用此函数来计算一定时间范围内的cpu平均值
    :param outputDic:
    :return:
    '''
    # print("正在处理设备cpu信息(单次)")
    cpu = []
    for line in outputDic:
        line = line.strip()
        cpuReg = re.compile('(Idle\\s*)(.*)%')
        endReg = re.compile('^MANAGEMENT:')
        matchCPU = re.match(cpuReg, line)  # 匹配CPU单核利用率
        endSign = re.match(endReg, line)  # 不统计managerment后续行
        if matchCPU:
            cpu.append(float(matchCPU.group(2)))
        if endSign:
            break
    cpuavr = mean(cpu)
    return "{:.1f}".format(100 - cpuavr)


def fwng23_meminfo(outputDic):
    '''
    根据单次获取的cpu值计算平均利用率，通过多次调用此函数来计算一定时间范围内的cpu平均值
    :param outputDic:
    :return:
    '''
    # print("正在处理设备cpu信息(单次)")
    diskutil = 0
    memutil = 0
    ismatch = 0
    for line in outputDic:
        line = line.strip()
        diskutilReg = re.compile('^(SE.*)(\\d+\\%)')
        memutilReg = re.compile('^MEM:.*')
        matchdiskutil = re.match(diskutilReg, line)  # 匹配CPU单核利用率
        matchmemC = re.match(memutilReg, line)  # 不统计managerment后续行
        if matchdiskutil:
            diskutil = matchdiskutil.group(2)
        if ismatch:
            memutil = line.split()[-1]
            ismatch = 0
        if matchmemC:
            ismatch = 1

    return diskutil, memutil


def fwng23_interface(outputDic):
    # 返回值中每个接口的列表第一项为RX的值，第二项为TX的值
    iscurint = 0
    isRX = 0
    isTX = 0
    interface = locals()  # 用以动态生成dic{}
    for line in outputDic:
        line = line.strip()
        interfaceReg = re.compile('.*(feth\\d{1,2}):\\sStart\\sUp.*')
        packetRXReg = re.compile('.*RX:\\sbytes.*')  # RX
        packetTXReg = re.compile('.*TX:\\sbytes.*')  # TX
        matchInter = re.match(interfaceReg, line)
        matchRX = re.match(packetRXReg, line)
        matchTX = re.match(packetTXReg, line)
        # print(matchInter)
        if matchInter:
            iscurint = 1
            interfaceName = matchInter.group(1)
        if iscurint == 1:  # 过滤掉不需要的接口信息
            if isRX == 1:  # 为了获取RX: bytes或者TX： bytes的下一行数值，通过isRX/TX来进行标记
                packetRX = line
                isRX = 0
            if matchRX:
                isRX = 1
            if isTX == 1:  # 匹配到TX: bytes的下一行即完成接口信息的采集
                packetTX = line
                isTX = 0
                iscurint = 0
                interface[interfaceName] = []
                interface[interfaceName].append(packetRX)
                interface[interfaceName].append(packetTX)
            if matchTX:
                isTX = 1
    # print(interface)
    for inter in list(interface.keys()):  # 将local()中生成的无关变量去除掉
        if not inter.startswith('feth'):
            interface.pop(inter)
    return(interface)


def fwng23_snmp(outputDic):
    SNMPValue = ''
    for line in outputDic:
        line = line.strip()
        SNMPReg = re.compile('^(snmpd\\sis\\s)(.*)!.*')
        matchSNMP = re.match(SNMPReg, line)
        if matchSNMP:
            SNMPValue = matchSNMP.group(2)
    return SNMPValue


def fwng23_dhcp(outputDic):
    for line in outputDic:
        line = line.strip()
        DHCPReg = re.compile('^(DHCP server mode\\s)(.*)(,interface.*)')
        matchDHCP = re.match(DHCPReg, line)  # 匹配DHCP配置
        if matchDHCP:
            DHCPValue = matchDHCP.group(2)
            return DHCPValue
    if not matchDHCP:  # 如果没有开启DHCP正则是匹配不到值的
        return 'not running'


def fwng23_natnum(outputDic):
    newstr = ''
    for perline in outputDic:
        newstr = newstr + perline.strip()
    natReg = re.compile('nat\spolicy\sadd')
    natSearch = natReg.findall(newstr)
    return len(natSearch)

def fwng23_policynum(outputDic):
    natValue = ''
    for line in outputDic:
        policyReg = re.compile('(total:)(.*)')
        matchPolicy = re.match(policyReg, line.strip())  # 匹配访问控制策略数量
        if matchPolicy:
            natValue = matchPolicy.group(2)
            break
    return natValue


def fwng23_logstat(outputDic):
    isdrop = 0
    isfail = 0
    count = 0
    for line in outputDic:
        line = line.strip()

        packetDropReg = re.compile('.*droped*')  # RX
        packetFailReg = re.compile('.*failed*')  # TX
        packetrTotalReg = re.compile('.*total:\\[(\\d+)\\]*')  # TX
        matchDrop = re.match(packetDropReg, line)
        matchFail = re.match(packetFailReg, line)
        matchTotal = re.match(packetrTotalReg, line)

        if isdrop == 1 and count == 2:
            dropNum = matchTotal.group(1)
            isdrop = 0
        if matchDrop:
            isdrop = isdrop + 1
            count = 0
        if isfail == 1 and count == 2:
            failnum = matchTotal.group(1)
            isfail = 0
        if matchFail:
            isfail = isfail + 1
            count = 0
        count = count + 1
    return dropNum, failnum


def fwng23_devname(outputDic):
    nameValue = ''
    for line in outputDic:
        nameReg = re.compile('(system\\sdevname\\sset\\s)(.*)')
        matchName = re.match(nameReg, line.strip())
        if matchName:
            nameValue = matchName.group(2)
    return nameValue


def fwng23_webui(outputDic):
    webValue = ''
    for line in outputDic:
        webReg = re.compile('^(system\\swebui\\sidle-timeout\\s)(.*)')
        matchWeb = re.match(webReg, line.strip())
        if matchWeb:
            webValue = matchWeb.group(2)
            break
    return webValue


def fwng23_servicestatus(outputDic):
    serviceStatusValue = {}
    for line in outputDic:
        serviceReg = re.compile('^(system\\sservice\\s)(.*)\\s(.*)')
        matchService = re.match(serviceReg, line.strip())
        if matchService:
            serviceStatusValue[matchService.group(2)] = matchService.group(3)
    return serviceStatusValue


def fwng23_localservice(outputDic):
    newstr = ''
    for perline in outputDic:
        newstr = newstr + perline.strip()
    splitReg = 'ID\s\d+\s'
    outputDicNew = re.split(splitReg, newstr)
    localservice = {}
    localservice['webui'] = []
    localservice['ssh'] = []
    localservice['telnet'] = []
    for line in outputDicNew:
        # serviceReg=re.compile('^(ID.*name\s)([a-zA-Z]*).*addressname\s(.*?)\s.*')
        serviceReg = re.compile(
            '^(.*name\s)([a-zA-Z]*).*\s(area){0,1}(.*)\saddressname\s(\S+)')
        matchService = re.match(serviceReg, line.strip())
        if matchService:
            if matchService.group(2) == 'webui':
                localservice['webui'].append([matchService.group(4), matchService.group(5)])
            elif matchService.group(2) == 'ssh':
                localservice['ssh'].append([matchService.group(4), matchService.group(5)])
            elif matchService.group(2) == 'telnet':
                localservice['telnet'].append([matchService.group(4), matchService.group(5)])
    return localservice


def fwng23_ALG(outputDic):
    algValue = {}
    for line in outputDic:
        algReg = re.compile('^(alg.*protocol\\s)(.*)\\senable\\s(.*)')
        matchALG = re.match(algReg, line.strip())
        if matchALG:
            algValue[matchALG.group(2)] = matchALG.group(3)
    return algValue


def fwng23_session(outputDic):
    intergrityValue = ''
    closeValue = ''
    otherValue = ''
    handsharkValue = ''
    udpValue = ''
    tcpestValue = ''
    # print("正在获取连接完整性配置及各连接超时时间")
    for line in outputDic:
        line = line.strip()
        integrityReg = re.compile(
            '^(network\\ssession\\ssession-integrity\\s)(.*)')
        # defaultReg=re.compile(('^(network\ssession\stimeout\sdefault)(.*)'))
        closeReg = re.compile(
            ('^(network\\ssession\\stimeout\\sclose\\s)(.*)'))
        otherReg = re.compile('^(network\\ssession\\stimeout\\sother\\s)(.*)')
        handsharkReg = re.compile(
            '^(network\\ssession\\stimeout\\shandshake\\s)(.*)')
        udpReg = re.compile('^(network\\ssession\\stimeout\\sudp\\s)(.*)')
        estavlishedReg = re.compile(
            '^(network\\ssession\\stimeout\\sestablished\\s)(.*)')
        matchIntegrity = re.match(integrityReg, line)  # 匹配连接完整性设置
        matchClose = re.match(closeReg, line)  # 匹配已关闭的tcp连接超时时间
        matchOther = re.match(otherReg, line)  # 匹配其他连接超时时间(如icmp)
        matchHandshark = re.match(handsharkReg, line)  # 匹配已握手tcp超时时间
        matchUDP = re.match(udpReg, line)  # 匹配UDP超时时间
        matchEstavilished = re.match(estavlishedReg, line)  # 匹配tcp已建立连接超时时间
        if matchIntegrity:
            intergrityValue = matchIntegrity.group(2)
        if matchClose:
            closeValue = matchClose.group(2)
        if matchOther:
            otherValue = matchOther.group(2)
        if matchHandshark:
            handsharkValue = matchHandshark.group(2)
        if matchUDP:
            udpValue = matchUDP.group(2)
        if matchEstavilished:
            tcpestValue = matchEstavilished.group(2)
    return intergrityValue, closeValue, otherValue, handsharkValue, udpValue, tcpestValue


def fwng23_authpolicy(outputDic):
    complexity = ''
    length = ''
    failtimes = ''
    locktime = ''
    for line in outputDic:
        line = line.strip()
        complexityReg = re.compile('^(password-complexity:)(.*)')
        lengthReg = re.compile('^(password-min-length:)(.*)')
        failtimeReg = re.compile('^(maxnum-auth-fail:)(.*)')
        locktimeReg = re.compile('^(account-locked-time:)(.*)')
        matchcom = re.match(complexityReg, line)  # 匹配管理员密码复杂度
        matchlength = re.match(lengthReg, line)  #
        matchfail = re.match(failtimeReg, line)  #
        matchlock = re.match(locktimeReg, line)  #
        if matchcom:
            complexity = matchcom.group(2)
        if matchlength:
            length = matchlength.group(2)
        if matchfail:
            failtimes = matchfail.group(2)
        if matchlock:
            locktime = matchlock.group(2)
    return complexity, length, failtimes, locktime


def fwng23_ha(outputDic):
    modeValue = 'as'
    statusValue = ''
    assetValue = '未配置 未配置 未配置 未配置'
    for line in outputDic:
        line = line.strip()
        statusReg = re.compile('^(HA-Status:\\s)(.*)')
        modespReg = re.compile('^(ha\\smode\\sset\\s)(.*).*')
        modeasReg = re.compile('(ACTIVE|STANDBY|INIT).*')
        matchStatus = re.match(statusReg, line)
        matchModeSP = re.match(modespReg, line)
        matchModeAS = re.match(modeasReg, line)
        if matchStatus:
            statusValue = matchStatus.group(2)
            continue
        if matchModeSP:
            modeValue = matchModeSP.group(2)
        if matchModeAS:
            assetValue = matchModeAS.group(0)

    return statusValue, modeValue, assetValue


def fwng23_log(outputDic):
    newstr = ''
    for perline in outputDic:
        newstr = newstr + perline.strip()
    splitReg = 'log\\sconfig\\s'
    outputDicNew = re.split(splitReg, newstr)
    logAddrValue = ''
    logTranValue = ''
    loglocal = ''
    loglevel = {}
    for line in outputDicNew:
        line = line.strip()
        logReg = re.compile(
            '(set\\sipaddr\\s\')(.*)\'\\s.*trans\\s([a-z]*)\\s.*')
        loglevelReg = re.compile(
            '(type\\s)(.*?)\\slevel\\s(\\d)')
        localReg = re.compile('(set\\slocal-database\\s)(.*)')
        matchLog = re.match(logReg, line)  # 匹配日志服务器地址及是否外发
        matchlocal = re.match(localReg, line)  # 是否存储本地
        matchloglevel = re.match(loglevelReg, line)
        if matchLog:
            logAddrValue = matchLog.group(2).split()
            logTranValue = matchLog.group(3)
        if matchlocal:
            loglocal = matchlocal.group(2)
        if matchloglevel:
            loglevel[matchloglevel.group(2)] = matchloglevel.group(3)
    return logAddrValue, logTranValue, loglocal, loglevel


if __name__ == '__main__':
    # 测试用
    import json
    import re
    from numpy import *
    outputDic = {
        'customname': 'test',
        'type': 'fwng23',
        'hostname': '10.8.195.13',
        'port': '22',
        'username': 'superman',
        'password': 'Talent!23195.13',
        'model': [
            'system product model\r\n',
            'NGFW4000-UF(T2E)\r\n',
            ' \r\n'],
        'version': [
            'system version\r\n',
            'VERSION: V3.2294.23096_NGFW.1_B\r\n',
            '\rPSN: Unknow\r\n',
            ' \r\n'],
        'sysinfo': [
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            6.0%\r\n',
            '    Idle            94.0%\r\n',
            '  nae2:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            24.5%\r\n',
            '    Idle            75.5%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       0\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            '  nae2:\r\n',
            '    Used            6.0%\r\n',
            '    Idle            94.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            9.8%\r\n',
            '    Idle            90.2%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       2\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            3.0%\r\n',
            '    Idle            97.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.9%\r\n',
            '    Idle            91.1%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       0\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.1%\r\n',
            '    Idle            91.9%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       2\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            '  nae2:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.7%\r\n',
            '    Idle            91.3%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 59 rate=       0\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            3.0%\r\n',
            '    Idle            97.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            6.0%\r\n',
            '    Idle            94.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.6%\r\n',
            '    Idle            91.4%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       2\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            7.8%\r\n',
            '    Idle            92.2%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 59 rate=       0\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            7.0%\r\n',
            '    Idle            93.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.4%\r\n',
            '    Idle            91.6%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       2\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae2:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            9.3%\r\n',
            '    Idle            90.7%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 59 rate=       0\r\n',
            ' \r\n',
            'system information\r\n',
            'CPU Information:\r\n',
            'NAE:\r\n',
            '  nae0:\r\n',
            '    Used            4.0%\r\n',
            '    Idle            96.0%\r\n',
            '  nae1:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            '  nae2:\r\n',
            '    Used            5.0%\r\n',
            '    Idle            95.0%\r\n',
            'MANAGEMENT:\r\n',
            '  cpu0:\r\n',
            '    Used            8.3%\r\n',
            '    Idle            91.7%\r\n',
            'Session Numbers Information:\r\n',
            '  Totalnum= 61 rate=       2\r\n',
            ' \r\n'],
        'meminfo': [
            'system utilization\r\n',
            '\r\n',
            'DISK:  Name            Total           Used            Free            Util  \r\r\n',
            '       DATA            98.7M           43.5M           50.1M           47%   \r\r\n',
            '       SE              117.4G          403.5M          111.0G          2%    \r\r\n',
            '       NGTOS-UPT       694.8M          435.2M          224.3M          66%   \r\r\n',
            'MEM:   Total           Used            Free            Util                  \r\r\n',
            '       7.7G            2.9G            4.8G            37%                   \r\r\n',
            ' \r\n'],
        'interface': [
            'network debug feth-stats\r\n',
            '13: feth999: Start Up FD @ 10000 Mbit/s  00:07:07:07:07:07\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            '14: feth0: Start Up FD @ 100 Mbit/s  00:16:31:f4:c4:9e\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  4372592321      32084953     0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  2112904006      5393354      0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            '15: feth1: Start Down  00:16:31:f4:c4:9f\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            '16: feth2: Start Down  00:16:31:f4:c4:a0\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            '17: feth3: Start Down  00:16:31:f4:c4:a1\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            '18: feth4: Start Down  00:16:31:f4:c4:a2\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            '19: feth5: Start Down  00:16:31:f4:c4:a3\r\n',
            '  RX: bytes       packets      errors       dropped   mcast\r\n',
            '  0               0            0            0         0           \r\n',
            '  TX: bytes       packets      errors    FDIR: match     miss\r\n',
            '  0               0            0         0               0              \r\n',
            'Priv_desc:\r\n',
            'RX-dropped(RX mbuf allocation failures)\t:\t0\r\n',
            'RX-errors:\r\n',
            'crcerrs(CRC Error Count)\r\t\t\t\t\t:\t0\r\n',
            'rlec(Receive Length Error Count)\r\t\t\t\t\t:\t0\r\n',
            'ruc(Receive Undersize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'roc(Receive Oversize Error Count)\r\t\t\t\t\t:\t0\r\n',
            'mpc(Missed Packets Count)\r\t\t\t\t\t:\t0\r\n',
            'rxerrc(RX Error Count)\r\t\t\t\t\t:\t0\r\n',
            'algnerrc(Alignment Error Count)\r\t\t\t\t\t:\t0\r\n',
            'cexterr(Carrier Extension Error Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n',
            'TX-errors:\r\n',
            'ecol(Excessive Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            'latecol(Late Collisions Count)\r\t\t\t\t\t:\t0\r\n',
            '\r\n'],
        'devname': [
            'system devname show\r\n',
            'system devname set TopsecOS\r\n',
            ' \r\n'],
        'uptime': [
            'system uptime\r\n',
            'UP 79 days, 04:02:12\r\n',
            ' \r\n'],
        'time': [
            'system time show\r\n',
            '+08 2022-03-18 14:02:41\r\n',
            ' \r\n'],
        'policynum': [
            'firewall policy total\r\n',
            'total:6\r\n',
            ' \r\n'],
        'natnum': [
            'nat policy show\r\n',
            "ID 31026 duplex nat policy add name nat-317134421 orig_src '172.22.0.56 ' orig_dst '119.253.86.218 ' trans_src 防火墙与128.7方向相连段IP tran\r\n",
            's_dst 10.2.128.7 hit-session 0 \r\n',
            ' \r\n'],
        'logstat': [
            'log stat\r\n',
            'log statistic:\r\n',
            'sended:\r\n',
            'mp:[5279] nae 1:[2862] nae 2:[1681] nae 3:[0]\r\n',
            'total:[9822]\r\n',
            '\r\n',
            'droped:\r\n',
            'mp:[0] nae 1:[0] nae 2:[0] nae 3:[0]\r\n',
            'total:[0]\r\n',
            '\r\n',
            'failed:\r\n',
            'mp:[0] nae 1:[0] nae 2:[0] nae 3:[0]\r\n',
            'total:[0]\r\n',
            ' \r\n'],
        'ntp': [
            'system ntp show\r\n',
            '\r\n',
            '=====  ntp状态信息 =====\r\n',
            '  模式 : 客户端\r\n',
            '  状态 : 自动同步服务运行中\r\n',
            '  服务器 : 10.10.101.1\r\n',
            '  ntp 时间 : 2022-03-18 13:53:25\r\n',
            '  连接结果 : 失败\r\n',
            '\r\n',
            '===== ntp 配置信息 =====\r\n',
            'system ntp set mode client server-address 10.10.101.1 \r\n',
            "system ntp set history-servers 'pool.ntp.org'\r\n",
            'system ntp start\r\n',
            'system ntp set auth disable\r\n',
            ' \r\n'],
        'webui': [
            'system webui show\r\n',
            'system webui idle-timeout 3600\r\n',
            'system webui port 443\r\n',
            'system webui verify-client-cert off\r\n',
            'system webui config-effect\r\n',
            ' \r\n'],
        'localservice': [
            'pf service show\r\n',
            'ID 17745 pf service add name ssh area MGMT addressname any policyname pfpolicy1 \r\n',
            'ID 17746 pf service add name telnet area MGMT addressname any policyname pfpolicy2 \r\n',
            'ID 17747 pf service add name webui area MGMT addressname 192.168.1.0 policyname pfpolicy3 \r\n',
            'ID 17748 pf service add name ping area MGMT addressname any policyname pfpolicy4 \r\n',
            'ID 17749 pf service add name webui area MGMT addressname any policyname pfpolicy5 \r\n',
            'ID 17750 pf service add name snmp area MGMT addressname any policyname pfpolicy6 \r\n',
            'ID 16706 pf service add name tp addressname any policyname pfpolicy7 \r\n',
            'ID 20714 pf service add name webui area trust addressname any policyname pfpolicy12 \r\n',
            'ID 21783 pf service add name ping area internet addressname any policyname 1 \r\n',
            'ID 21784 pf service add name webui area internet addressname any policyname pf21011357 \r\n',
            'ID 21785 pf service add name ssh area internet addressname any policyname pf21011415 \r\n',
            'ID 21831 pf service add name webui area MGMT addressname H_10.67.40.45 policyname pf222172646 \r\n',
            'ID 31028 pf service add name webui  addressname any policyname pf317152005 \r\n',
            ' \r\n'],
        'servicestatus': [
            'system service status\r\n',
            'system service telnetd off\r\n',
            'system service sshd on\r\n',
            'system service httpd on\r\n',
            ' \r\n'],
        'ALG': [
            'alg config show\r\n',
            'alg config set protocol ftp enable yes\r\n',
            'alg config set protocol tftp enable yes\r\n',
            'alg config set protocol sqlnet enable yes\r\n',
            'alg config set protocol pptp enable yes\r\n',
            'alg config set protocol sip enable yes\r\n',
            'alg config set protocol h323 enable yes\r\n',
            'alg config set protocol rtsp enable yes\r\n',
            'alg session-expect-num set 0\r\n',
            ' \r\n'],
        'session': [
            'network session show configuration\r\n',
            'network session timeout deny 10\r\n',
            'network session timeout close 20\r\n',
            'network session timeout other 20\r\n',
            'network session timeout handshake 20\r\n',
            'network session timeout udp 60\r\n',
            'network session timeout established 1800\r\n',
            'network session timeout never-expire 86400\r\n',
            'network session timeout ha-expire 7200\r\n',
            'network session not_same_cpu_drop on\r\n',
            'network session timer delete 1024\r\n',
            'network session reclaim delete 16\r\n',
            'network session defrag on\r\n',
            'network session icmp-redirect off\r\n',
            'network session icmpv6-redirect off\r\n',
            'network session close_timer timeout 512 del_nums 64\r\n',
            'network session tcp-reset off\r\n',
            'network session tcp-reset listen_mode send_count  10 ack_step 64 \r\n',
            'network session session-integrity on\r\n',
            'network session only-syn-create on\r\n',
            'network session packet-checksum off\r\n',
            'network session syn-reset off\r\n',
            'network session reclaim all\r\n',
            'network session never-expire-percent 10\r\n',
            '\r                                 \rnetwork session udp_never_expire on\r\n',
            ' \r\n'],
        'log': [
            'log config show\r\n',
            "log config set ipaddr '192.168.1.253' port UDP:514 logtype welf trans enable trans_gather no trans_coding utf8\r\n",
            'log config crypt disable\r\n',
            'log config set console off\r\n',
            'log config set local-database on\r\n',
            'log config set dp-switch off\r\n',
            'log config set log-send-interval 0\r\n',
            'log config log_time local\r\n',
            'log config type streamav level 6\r\n',
            'log config type streamav table-capacity 100\r\n',
            'log config type ac level 6\r\n',
            'log config type ac table-capacity 100\r\n',
            'log config type ctrlsess level 8\r\n',
            'log config type ctrlsess table-capacity 100\r\n',
            'log config type apt level 8\r\n',
            'log config type apt table-capacity 100\r\n',
            'log config type file_block level 8\r\n',
            'log config type file_block table-capacity 100\r\n',
            'log config type url_filter level 8\r\n',
            'log config type url_filter table-capacity 100\r\n',
            'log config type data_filter level 8\r\n',
            'log config type data_filter table-capacity 100\r\n',
            'log config type ipmac level 8\r\n',
            'log config type ipmac table-capacity 100\r\n',
            '\r                                 \rlog config type pf_rule level 6\r\n',
            'log config type pf_rule table-capacity 100\r\n',
            'log config type abnormal_threat level 8\r\n',
            'log config type abnormal_threat table-capacity 100\r\n',
            'log config type ids level 8\r\n',
            'log config type ids table-capacity 100\r\n',
            'log config type mgmt level 6\r\n',
            'log config type mgmt table-capacity 100\r\n',
            'log config type admin level 6\r\n',
            'log config type admin table-capacity 100\r\n',
            'log config type admin_config level 6\r\n',
            'log config type admin_config table-capacity 100\r\n',
            'log config type user_auth level 8\r\n',
            'log config type user_auth table-capacity 100\r\n',
            'log config type firmware_update level 8\r\n',
            'log config type firmware_update table-capacity 100\r\n',
            'log config type license_update level 8\r\n',
            'log config type license_update table-capacity 100\r\n',
            'log config type topguard level 8\r\n',
            'log config type topguard table-capacity 100\r\n',
            'log config type rules_update level 8\r\n',
            'log config type rules_update table-capacity 100\r\n',
            'log config type ips level 6\r\n',
            '\r                                 \rlog config type ips table-capacity 100\r\n',
            'log config type ads_clean level 8\r\n',
            'log config type ads_clean table-capacity 100\r\n',
            'log config type ha level 7\r\n',
            'log config type ha table-capacity 100\r\n',
            'log config type interface level 8\r\n',
            'log config type interface table-capacity 100\r\n',
            'log config type vpn level 8\r\n',
            'log config type vpn table-capacity 100\r\n',
            'log config type spam level 8\r\n',
            'log config type spam table-capacity 100\r\n',
            'log config type cpu_monitor level 0\r\n',
            'log config type cpu_monitor table-capacity 100\r\n',
            'log config type memory_monitor level 0\r\n',
            'log config type memory_monitor table-capacity 100\r\n',
            'log config type disk_monitor level 0\r\n',
            'log config type disk_monitor table-capacity 100\r\n',
            'log config type recover level 8\r\n',
            'log config type recover table-capacity 100\r\n',
            'log config type cputemp_monitor level 8\r\n',
            'log config type cputemp_monitor table-capacity 100\r\n',
            'log config type systemp_monitor level 8\r\n',
            'log config type systemp_monitor table-capacity 100\r\n',
            '\r                                 \rlog config type session level 8\r\n',
            'log config type session table-capacity 100\r\n',
            'log config type pf level 8\r\n',
            'log config type pf table-capacity 100\r\n',
            'log config type neighbour level 8\r\n',
            'log config type neighbour table-capacity 100\r\n',
            'log config type linkbak level 8\r\n',
            'log config type linkbak table-capacity 100\r\n',
            'log config type nat level 8\r\n',
            'log config type nat table-capacity 100\r\n',
            'log config type session_warning level 8\r\n',
            'log config type session_warning table-capacity 100\r\n',
            ' \r\n'],
        'authpolicy': [
            'system admin-auth-policy show\r\n',
            'password-complexity: medium\r\n',
            'password-min-length: 12\r\n',
            'anti-crack: on\r\n',
            'locked-time-sum: no \r\n',
            'maxnum-auth-fail: 5\r\n',
            'account-locked-time: 60(seconds)\r\n',
            'maxnum-admin-online: 100\r\n',
            'maxnum-same-admin-online: 10\r\n',
            'excend-session-process-poilcy: deny\r\n',
            'password-need-change-first-login: yes\r\n',
            'webui-code: yes \r\n',
            'online number limit of login type: \r\n',
            'webui: 10 ;ssh: 10 ;telnet: 10 \r\n',
            'password modify period: 0 days\r\n',
            'auth method: \r\n',
            'external shift-local-auth on,server name: Tacacs \r\n',
            ' \r\n'],
        'ha': [
            'ha show status\r\n',
            'ha mode set sp\r\n',
            'HA-Status: ha disable\r\n',
            'Heartbeat-Local IP:\r\n',
            'Heartbeat-Remote IP\r\n',
            ':\r\n',
            'Group 1\r\n',
            'State       Preempt    Priority   Interface \r\n',
            'INIT        disable    65000      feth0,feth3,feth4\r\n',
            '\r\n',
            'Group 2\r\n',
            'State       Preempt    Priority   Interface \r\n',
            'INIT        disable    65000      \r\n',
            ' \r\n'],
        'snmp': [
            'system snmp status show\r\n',
            'snmpd is not running!\r\n',
            ' \r\n']}
    model = fwng23_model(outputDic['model'])
    # print(json.dumps(outputDic,indent=4))
    (ver, psn) = fwng23_version(outputDic['version'])  # 已测试
    meminfo = fwng23_meminfo(outputDic['meminfo'])  # 内存、硬盘
    # cpuinfo = =fwng23_cpuinfo(outputDic['sysinfo'])cpu暂不在此测试
    interface = fwng23_interface(outputDic['interface'])
    name = fwng23_devname(outputDic['devname'])
    uptime = fwng23_uptime(outputDic['uptime'])
    time = fwng23_time(outputDic['time'])
    policy = fwng23_policynum(outputDic['policynum'])
    nat = fwng23_natnum(outputDic['natnum'])
    logstat = fwng23_logstat(outputDic['logstat'])
    ntp = fwng23_ntp(outputDic['ntp'])
    webui = fwng23_webui(outputDic['webui'])
    localservice = fwng23_localservice(outputDic['localservice'])
    servicestatus = fwng23_servicestatus(outputDic['servicestatus'])
    ALG = fwng23_ALG(outputDic['ALG'])
    intergrityValue = fwng23_session(outputDic['session'])
    log = fwng23_log(outputDic['log'])
    auth = fwng23_authpolicy(outputDic['authpolicy'])
    ha = fwng23_ha(outputDic['ha'])
    snmp = fwng23_snmp(outputDic['snmp'])
    #cpuavg = fwng23_sysinfo(outputDic['sysinfo'])

    print(
        model,
        ver,
        psn,
        meminfo,
        interface,
        name,
        uptime,
        time,
        policy,
        nat,
        logstat,
        ntp,
        webui,
        servicestatus,
        localservice,
        intergrityValue,
        log,
        ALG,
        auth,
        ha,
        snmp,
    )
