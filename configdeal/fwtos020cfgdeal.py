#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: ngcondeal
# @Function:
# @Author: RyneZ
# @Time: 2021/4/14 11:52
import re
from numpy import *
import time


def fwtos020_version(outputDic):
    version = ''
    PSN = ''
    # print("正在获取设备系统版本及序列号")
    for line in outputDic:
        # print(line.strip())
        line = line.strip()
        verReg = re.compile('^(.*?tos_)(.*)')
        psnReg = re.compile('^(.*?)(\\S\\d+)')
        matchVer = re.match(verReg, line)  # 匹配版本号
        matchPSN = re.match(psnReg, line)  # 匹配序列号
        if matchVer:
            version = matchVer.group(2)
            continue
        if matchPSN:
            PSN = matchPSN.group(2)
            continue
    return version, PSN


def fwtos020_time(outputDic):

    line = outputDic[1].strip()
    times = line.split()
    devicetime = times[1] + ' ' + times[2]
    localtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    return devicetime, localtime


def fwtos020_ntp(outputDic):
    address = ''
    status = 'stopped'
    # print(outputDic)
    for line in outputDic:
        line = line.strip()
        ntpaddress1Reg = re.compile('.*server\\sip:\\s(\\d+.\\d+.\\d+.\\d+)')
        ntpaddress2Reg = re.compile(
            '.*server\\sip1:\\s(\\d+.\\d+.\\d+.\\d+)\\s+ip2:\\s(\\d+.\\d+.\\d+.\\d+)')
        matchaddress1 = re.match(ntpaddress1Reg, line)
        matchaddress2 = re.match(ntpaddress2Reg, line)
        if matchaddress1:
            address = matchaddress1.group(1)
            status = 'running'
        if matchaddress2:
            address = matchaddress2.group(1) + ',' + matchaddress2.group(2)
            status = 'running'
    return address, status


def fwtos020_uptime(outputDic):
    # print("正在处理设备运行天数")

    uptime = 0
    for line in outputDic:
        line = line.strip()
        uptimeReg = re.compile('^(UP )(.*)(days.*)')
        matchtime = re.match(uptimeReg, line)  # 匹配系统运行天数
        if matchtime:
            uptime = matchtime.group(2)
    return uptime


def fwtos020_authpolicy(outputDic):
    complexity = ''
    length = ''
    failtimes = ''
    locktime = ''
    for line in outputDic:
        line = line.strip()
        complexityReg = re.compile(
            '^(.*set-manager\\spolicy\\s)(.*)\\smin-length\\s(\\d*)')
        failtimeReg = re.compile('^(.*authfail\\sset\\smaxnum\\s)(\\d*)')
        locktimeReg = re.compile('^(.*faillock\\sset\\stime\\s)(.*)')
        matchcom = re.match(complexityReg, line)  # 匹配管理员密码复杂度
        matchfail = re.match(failtimeReg, line)  #
        matchlock = re.match(locktimeReg, line)  #
        if matchcom:
            complexity = matchcom.group(2)
            length = matchcom.group(3)

        if matchfail:
            failtimes = matchfail.group(2)
        if matchlock:
            locktime = matchlock.group(2)
    return complexity, length, failtimes, locktime


def fwtos020_snmpcfg(outputDic):
    '''
    根据单次获取的cpu值计算平均利用率，通过多次调用此函数来计算一定时间范围内的cpu平均值
    :param outputDic:
    :return:
    '''
    # print("正在处理设备cpu信息(单次)")
    pass
    return 0


def fwtos020_sysinfo(outputDic):
    '''
    根据单次获取的cpu值计算平均利用率，通过多次调用此函数来计算一定时间范围内的cpu平均值
    :param outputDic:
    :return:
    '''
    # print("正在处理设备cpu信息(单次)")
    cpu = []
    for line in outputDic:
        line = line.strip()
        cpuReg = re.compile('(Total Used\\s*)(.*)%$')
        endReg = re.compile('^MANAGEMENT:')
        matchCPU = re.match(cpuReg, line)  # 匹配CPU单核利用率
        endSign = re.match(endReg, line)  # 不统计managerment后续行
        if matchCPU:
            cpu.append(float(matchCPU.group(2)))
        if endSign:
            break
    cpuavr = mean(cpu)
    return "{:.2f}".format(cpuavr)


def fwtos020_meminfo(outputDic):
    '''
    根据单次获取的cpu值计算平均利用率，通过多次调用此函数来计算一定时间范围内的cpu平均值
    :param outputDic:
    :return:
    '''
    # print("正在处理设备mem信息(单次)")
    mem = []
    for line in outputDic:
        line = line.strip()
        memReg = re.compile('(Free\'s ratio\\s*)(.*)%$')
        endReg = re.compile('^MANAGEMENT:')
        matchMEM = re.match(memReg, line)
        endSign = re.match(endReg, line)  # 不统计managerment后续行
        if matchMEM:
            mem.append(100 - float(matchMEM.group(2)))
        if endSign:
            break
    memavr = mean(mem)
    return "{:.2f}".format(memavr)


def fwtos020_model(outputDic):

    return outputDic[1].strip()


def fwtos020_lognum(outputDic):
    numReg = re.compile('Tot.*:\\s(\\d*)')

    for line in outputDic:
        line = line.strip()
        matchnum = re.match(numReg, line)
        if matchnum:
            return matchnum.group(1)


def fwtos020_area(outputDic):
    newstr = ''
    for perline in outputDic:
        newstr = newstr + perline.strip()
    splitReg = 'ID\s\d+\s'
    outputDicNew = re.split(splitReg, newstr)
    areaReg = re.compile(".*name\s(.*)\sattribute\s'(.*)'\saccess\s(.*)vsid")
    arealist = []
    for line in outputDicNew:
        line = line.strip()
        matcharea = re.match(areaReg, line)
        if matcharea:
            name=matcharea.group(1)
            interface = matcharea.group(2)
            access = matcharea.group(3)
            arealist.append([name,interface, access])
    return arealist


def fwtos020_neverexp(outputDic):
    allneverReg = re.compile('Allow All-never-expire:\\s+(\\d*)')
    neverexpReg = re.compile('All-never-expire:\\s+(\\d*)')
    estabReg = re.compile('Estab-never-expire:\\s+(\\d*)')
    arealist = []
    for line in outputDic:
        line = line.strip()
        matchallnever = re.match(allneverReg, line)
        matchneverexp = re.match(neverexpReg, line)
        matchestab = re.match(estabReg, line)
        if matchallnever:
            allnevervalue = matchallnever.group(1)
        if matchneverexp:
            neverexpvalue = matchneverexp.group(1)
        if matchestab:
            estabvalue = matchestab.group(1)
    return allnevervalue, neverexpvalue, estabvalue


def fwtos020_interface(outputDic):
    rx0sign = 0
    tx0sign = 0
    iscurint = 0
    interface = locals()  # 用以动态生成dic{}
    interfaceReg = re.compile('^(eth\\d*)\\s.*')
    endReg = re.compile('.*RX\\sbytes.*')  # DROP
    packetRXReg = re.compile(
        '.*RX packets:(\\d*).*errors:(\\d*).*dropped:(\\d*).*overruns:(.*)\\sframe:(.*)')  # RX
    packetTXReg = re.compile(
        '.*TX packets:(\\d*).*errors:(\\d*).*dropped:(\\d*).*overruns:(.*)\\scarrier:(.*)')  # TX
    for line in outputDic:
        line = line.strip()
        # print(line)
        matchInter = re.match(interfaceReg, line)
        matchRX = re.match(packetRXReg, line)
        matchTX = re.match(packetTXReg, line)
        matchEnd = re.match(endReg, line)
        if matchInter:
            iscurint = 1
            interfaceName = matchInter.group(1)
        if iscurint == 1:  # 过滤掉不需要的接口信息
            if matchRX:
                if matchRX.group(1) == '0':
                    rx0sign = 1
                packetRXToatal = matchRX.group(1)
                packetRXError = matchRX.group(2)
                packetRXDrop = matchRX.group(3)
                packetRXOverruns = matchRX.group(4)
                packetRXFrame = matchRX.group(5)
            if matchTX:
                if matchTX.group(1) == '0':
                    tx0sign = 1
                packetTXToatal = matchTX.group(1)
                packetTXError = matchTX.group(2)
                packetTXDrop = matchTX.group(3)
                packetTXOverruns = matchTX.group(4)
                packetTXCarrier = matchTX.group(5)
            if matchEnd:
                iscurint = 0
                if rx0sign == 1 and tx0sign == 1:
                    rx0sign = 0
                    tx0sign = 0
                    continue
                # print(interfaceName,packetToatal,packetTX)
                interface[interfaceName] = [[], []]
                interface[interfaceName][0].extend(
                    [packetRXToatal, packetRXError, packetRXDrop, packetRXOverruns, packetRXFrame])
                interface[interfaceName][1].extend(
                    [packetTXToatal, packetTXError, packetTXDrop, packetTXOverruns, packetTXCarrier])

    # print(list(interface.keys()))
    for inter in list(interface.keys()):  # 将local()中生成的无关变量去除掉
        if not inter.startswith('eth'):
            interface.pop(inter)
    return(interface)


def fwtos020_session(outputDic):
    intergrityValue = 'on'
    closeValue = 'default'
    otherValue = 'default'
    handsharkValue = 'default'
    udpValue = 'default'
    tcpestValue = 'default'
    # print("正在获取连接完整性配置及各连接超时时间")
    for line in outputDic:
        line = line.strip()
        integrityReg = re.compile(
            '^(network\\ssession\\ssession-integrity\\s)(.*)')
        # defaultReg=re.compile(('^(network\ssession\stimeout\sdefault)(.*)'))
        closeReg = re.compile(
            ('^(network\\ssession\\stimeout\\sclose\\s)(.*)'))
        otherReg = re.compile('^(network\\ssession\\stimeout\\sother\\s)(.*)')
        handsharkReg = re.compile(
            '^(network\\ssession\\stimeout\\shandshake\\s)(.*)')
        udpReg = re.compile('^(network\\ssession\\stimeout\\sudp\\s)(.*)')
        estavlishedReg = re.compile(
            '^(network\\ssession\\stimeout\\sestablished\\s)(.*)')

        matchIntegrity = re.match(integrityReg, line)  # 匹配连接完整性设置
        matchClose = re.match(closeReg, line)  # 匹配已关闭的tcp连接超时时间
        matchOther = re.match(otherReg, line)  # 匹配其他连接超时时间(如icmp)
        matchHandshark = re.match(handsharkReg, line)  # 匹配已握手tcp超时时间
        matchUDP = re.match(udpReg, line)  # 匹配UDP超时时间
        matchEstavilished = re.match(estavlishedReg, line)  # 匹配tcp已建立连接超时时间

        if matchIntegrity:
            intergrityValue = matchIntegrity.group(2)
        if matchClose:
            closeValue = matchClose.group(2)
        if matchOther:
            otherValue = matchOther.group(2)
        if matchHandshark:
            handsharkValue = matchHandshark.group(2)
        if matchUDP:
            udpValue = matchUDP.group(2)
        if matchEstavilished:
            tcpestValue = matchEstavilished.group(2)

    return intergrityValue, closeValue, otherValue, handsharkValue, udpValue, tcpestValue


def fwtos020_snmp(outputDic):
    SNMPValue = ''
    for line in outputDic:
        line = line.strip()
        SNMPReg = re.compile('^(snmpd\\sis\\s)(.*)!.*')
        matchSNMP = re.match(SNMPReg, line)
        if matchSNMP:
            SNMPValue = matchSNMP.group(2)
    return SNMPValue


# def fwtos020_snmpcfg(outputDic):
#     SNMPcfg = []
#     for line in outputDic:
#         line = line.strip()
#         SNMPcfgReg = re.compile('^(.*?)\\s*(\\d\\.\\d\\.\\d\\.\\d)')
#         matchSNMPcfg = re.match(SNMPcfgReg, line)
#         if matchSNMPcfg:
#             SNMPhostname = matchSNMPcfg.group(1)
#             SNMPhostip = matchSNMPcfg.group(2)
#             SNMPcfg.append([SNMPhostip, SNMPhostname])
#     return SNMPcfg


# def fwtos020_dhcp(outputDic):
#     for line in outputDic:
#         line = line.strip()
#         DHCPReg = re.compile('^(DHCP server mode\\s)(.*)(,interface.*)')
#         matchDHCP = re.match(DHCPReg, line)  # 匹配DHCP配置
#         if matchDHCP:
#             DHCPValue = matchDHCP.group(2)
#             return DHCPValue
#     if not matchDHCP:  # 如果没有开启DHCP正则是匹配不到值的
#         return 'not running'


def fwtos020_policynum(outputDic):
    numValue = ''
    for line in outputDic:
        policyReg = re.compile('(total:)(.*)')
        matchPolicy = re.match(policyReg, line.strip())  # 匹配访问控制策略数量
        if matchPolicy:
            numValue = matchPolicy.group(2)
            break
    return numValue


def fwtos020_natnum(outputDic):
    natValue = ''
    for line in outputDic:
        policyReg = re.compile('(total:)(.*)')
        matchPolicy = re.match(policyReg, line.strip())  # 匹配nat数量
        if matchPolicy:
            natValue = matchPolicy.group(2)
            break
    return natValue


def fwtos020_log(outputDic):
    logAddrValue = ''
    logTranValue = ''
    loglocal = ''
    loglevel = ''
    for line in outputDic:
        line = line.strip()
        logReg = re.compile(
            '(log\\sserver\\sipaddr\\s\')(.*)\'')
        transReg = re.compile('(log\\stransmit\\soption\\s)(.*)')
        loglevelReg = re.compile('(log\\slevel:\\s)(\\d)')
        localReg = re.compile('(log\\stype:\\s)(.*)')
        matchLog = re.match(logReg, line)  # 匹配日志服务器地址
        matchTrans = re.match(transReg, line)  # 匹配日志是否外发
        matchlocal = re.match(localReg, line)  # 是否存储本地
        matchloglevel = re.match(loglevelReg, line)
        if matchLog:
            logAddrValue = matchLog.group(2).split()
        if matchTrans:
            logTranValue = matchTrans.group(2)
        if matchlocal:
            loglocal = matchlocal.group(2).split('|')
        if matchloglevel:
            loglevel = matchloglevel.group(2)
    return logAddrValue, logTranValue, loglocal, loglevel


def fwtos020_devname(outputDic):
    nameValue = ''
    for line in outputDic:
        nameReg = re.compile('(system\\sdevname\\sset\\s)(.*)')
        matchName = re.match(nameReg, line.strip())
        if matchName:
            nameValue = matchName.group(2)
    return nameValue


def fwtos020_webui(outputDic):
    webValue = ''
    for line in outputDic:
        webReg = re.compile('^(system\\swebui\\sidle-timeout\\s)(.*)')
        matchWeb = re.match(webReg, line.strip())
        if matchWeb:
            webValue = matchWeb.group(2)
            break
    return webValue


def fwtos020_servicestatus(outputDic):

    serviceStatusValue = {}
    for line in outputDic:
        serviceReg = re.compile('^(system\\s)(.*)\\s(start|stop)')
        matchService = re.match(serviceReg, line.strip())
        if matchService:
            serviceStatusValue[matchService.group(2)] = matchService.group(3)
    return serviceStatusValue


def fwtos020_localservice(outputDic):
    # 串口下一行返回太长的话会自动返回两行，需要先转换为一行才能进行下一步匹配
    newstr = ''
    for perline in outputDic:
        newstr = newstr + perline.strip()
    splitReg = 'ID\s\d+\s'
    outputDicNew = re.split(splitReg, newstr)
    localservice = {}
    localservice['webui'] = []
    localservice['ssh'] = []
    localservice['telnet'] = []
    for line in outputDicNew:
        # serviceReg=re.compile('^(ID.*name\s)([a-zA-Z]*).*addressname\s(.*?)\s.*')
        serviceReg = re.compile(
            '^(.*name\s)([a-zA-Z]*).*\sarea\s(.*)\saddressname\s(\S+)')
        matchService = re.match(serviceReg, line.strip())
        if matchService:
            if matchService.group(2) == 'webui':
                localservice['webui'].append([matchService.group(3),matchService.group(4)])
            elif matchService.group(2) == 'ssh':
                localservice['ssh'].append([matchService.group(3),matchService.group(4)])
            elif matchService.group(2) == 'telnet':
                localservice['telnet'].append([matchService.group(3),matchService.group(4)])
    return localservice


def fwtos020_ALG(outputDic):
    #串口下一行返回太长的话会自动返回两行，需要先转换为一行才能进行下一步匹配
    newstr=''
    for perline in outputDic:
        newstr=newstr+perline.strip()
    splitReg='ID\s\d+\s'
    outputDicNew=re.split(splitReg,newstr)
    algValue = []
    for line in outputDicNew:
        algReg = re.compile('^(.*)port\\s(.*)\\sname\\s(.*)\\senable\\s(.*)')
        matchALG = re.match(algReg, line.strip())
        #print(matchALG)
        if matchALG:
            algValue.append([matchALG.group(2),matchALG.group(3),matchALG.group(4)])
    return algValue


def fwtos020_mactable(outputDic):
    linknum = 0
    poolnum = 0
    for line in outputDic:
        macReg = re.compile('.*link:(\\d+)\\s+pool:(\\d+)')
        matchmac = re.match(macReg, line)
        if matchmac:
            linknum = matchmac.group(1)
            poolnum = matchmac.group(2)
    return linknum, poolnum


def fwtos020_hacfg(outputDic):
    pass


def fwtos020_ha(outputDic):
    modeValue = 'as'  # 默认主备
    statusValue = ''
    assetValue = []
    localaddr = ''
    for line in outputDic:
        line = line.strip()
        statusReg = re.compile('^(ha\\s)(enable|disable)')
        modeReg = re.compile('^(ha\\smode\\s)(.*)')
        localReg = re.compile('^(ha\\slocal\\s)(.*)')
        modeasReg = re.compile('^\\d\\.\\d\\.\\d\\.\\d.*')
        matchStatus = re.match(statusReg, line)
        matchLocal = re.match(localReg, line)
        matchMode = re.match(modeReg, line)
        matchModeAS = re.match(modeasReg, line)
        if matchStatus:
            statusValue = matchStatus.group(2)
            continue
        if matchLocal:
            localaddr = matchLocal.group(2)
        if matchMode:
            modeValue = matchMode.group(2)
        if matchModeAS:
            assetValue.append(matchModeAS.group(0).split())

    return statusValue, modeValue, assetValue, localaddr


if __name__ == '__main__':
    # 测试用
    import json
    import re
    from numpy import *
    outputDic={'customname': '核心防火墙-10.28.30.12', 'type': 'fwtos020', 'hostname': 'COM3', 'port': '9600', 'username': 'superman', 'password': 'Talent@123456', 'model': ['system product show', 'error -100110 : 无效输入，分析错误'], 'version': ['system version', 'VERSION: V3.2073N.2060P_NGFW.1', 'PSN: Q1711606564'], 'time': ['system time show', '+08 2022-05-09 13:39:42'], 'ntp': ['system ntp show', 'Ntp stopped'], 'uptime': ['system uptime', 'UP 13 days, 02:07:28'], 'sysinfo': ['system information', 'CPU Information:', 'manager:', 'Used            1.0%', 'Idle            99.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            1.0%', 'Idle            99.0%', 'Session Numbers Information:', 'Totalnum= 2930 rate=      47', 'system information', 'CPU Information:', 'manager:', 'Used            1.0%', 'Idle            99.0%', 'nae0:', 'Used            2.0%', 'Idle            98.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2909 rate=      50', 'system information', 'CPU Information:', 'manager:', 'Used            2.0%', 'Idle            98.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            1.0%', 'Idle            99.0%', 'Session Numbers Information:', 'Totalnum= 2874 rate=      25', 'system information', 'CPU Information:', 'manager:', 'Used            0.0%', 'Idle            100.0%', 'nae0:', 'Used            2.0%', 'Idle            98.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2857 rate=      48', 'system information', 'CPU Information:', 'manager:', 'Used            3.0%', 'Idle            97.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            1.0%', 'Idle            99.0%', 'nae2:', 'Used            1.0%', 'Idle            99.0%', 'Session Numbers Information:', 'Totalnum= 2930 rate=      48', 'system information', 'CPU Information:', 'manager:', 'Used            2.0%', 'Idle            98.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 3004 rate=     113', 'system information', 'CPU Information:', 'manager:', 'Used            0.0%', 'Idle            100.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            1.0%', 'Idle            99.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2933 rate=      54', 'system information', 'CPU Information:', 'manager:', 'Used            3.0%', 'Idle            97.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            1.0%', 'Idle            99.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2869 rate=      32', 'system information', 'CPU Information:', 'manager:', 'Used            1.0%', 'Idle            99.0%', 'nae0:', 'Used            2.0%', 'Idle            98.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2920 rate=      67', 'system information', 'CPU Information:', 'manager:', 'Used            1.0%', 'Idle            99.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            1.0%', 'Idle            99.0%', 'Session Numbers Information:', 'Totalnum= 2899 rate=      67'], 'meminfo': ['system information', 'CPU Information:', 'manager:', 'Used            3.0%', 'Idle            97.0%', 'nae0:', 'Used            1.0%', 'Idle            99.0%', 'nae1:', 'Used            2.0%', 'Idle            98.0%', 'nae2:', 'Used            2.0%', 'Idle            98.0%', 'Session Numbers Information:', 'Totalnum= 2846 rate=      60'], 'interface': ['network interface show', 'feth0     Link encap:Ethernet  HWaddr 00:13:32:0f:21:04', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet addr:192.168.1.254  Bcast:192.168.1.255  Mask:255.255.255.0', 'inet6 addr:fe80::213:32ff:fe0f:2104/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:2059 Tx packets:4100 Dropped:50', 'RX bytes:402325 (392.8 kb)  TX bytes:4608542 (4.3 Mb)', 'Vsys:root_vsys  Ifindex:87', 'Commtype:routing  Management Port', 'feth1     Link encap:Ethernet  HWaddr 00:13:32:0f:21:05', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:100.100.100.1  Bcast:100.100.100.255  Mask:255.255.255.0', 'inet6 addr:fe80::213:32ff:fe0f:2105/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:4541137 Tx packets:16397969 Dropped:0', 'RX bytes:240277008 (229.1 Mb)  TX bytes:13543540034 (12.6 Gb)', 'Vsys:root_vsys  Ifindex:88', 'Commtype:routing  Management Port', 'feth10    Description:内网', 'Link encap:Ethernet  HWaddr 00:10:f3:6f:b4:07', 'Link status: established, Autoneg enable', 'Full-duplex, 10000Mb/s', 'inet addr:10.28.30.12  Bcast:10.28.30.255  Mask:255.255.255.0', 'inet addr:10.28.30.22  Bcast:10.28.30.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6f:b407/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:588764593 Tx packets:1443380514 Dropped:94091353', 'RX bytes:334806846135 (311.8 Gb)  TX bytes:1734150900135 (1615.0 Gb)', 'Vsys:root_vsys  Ifindex:109', 'Commtype:routing', 'feth11    Link encap:Ethernet  HWaddr 00:10:f3:6f:b4:06', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6f:b406/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:108', 'Commtype:routing', 'feth12    Link encap:Ethernet  HWaddr 00:10:f3:6f:b4:05', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6f:b405/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:107', 'Commtype:routing', 'feth13    Link encap:Ethernet  HWaddr 00:10:f3:6f:b4:04', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6f:b404/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:106', 'Commtype:routing', 'feth20    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:13', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd13/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:104', 'Commtype:routing', 'feth21    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:14', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd14/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:105', 'Commtype:routing', 'feth22    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:11', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd11/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:102', 'Commtype:routing', 'feth23    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:12', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd12/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:103', 'Commtype:routing', 'feth24    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:0f', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd0f/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:100', 'Commtype:routing', 'feth25    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:10', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd10/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:101', 'Commtype:routing', 'feth26    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:0d', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd0d/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:98', 'Commtype:routing', 'feth27    Link encap:Ethernet  HWaddr 00:10:f3:6d:cd:0e', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:cd0e/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:99', 'Commtype:routing', 'feth30    Description:内网', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:d3', 'Link status:not established, Autoneg enable', 'Unknown-duplex, unknown speed', 'inet6 addr:fe80::210:f3ff:fe6d:d2d3/64  Scope:Link', 'UP BROADCAST MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:0 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)', 'Vsys:root_vsys  Ifindex:96', 'Commtype:routing', 'feth31    Description:人行', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:d4', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:9.59.195.136  Bcast:9.59.195.143  Mask:255.255.255.240', 'inet6 addr:fe80::210:f3ff:fe6d:d2d4/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:2638289 Tx packets:1714301 Dropped:7', 'RX bytes:3560327314 (3.3 Gb)  TX bytes:520059213 (495.9 Mb)', 'Vsys:root_vsys  Ifindex:97', 'Commtype:routing', 'feth32    Description:专员办', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:d1', 'Link status: established, Autoneg enable', 'Full-duplex, 100Mb/s', 'inet addr:172.31.254.1  Bcast:172.31.254.3  Mask:255.255.255.252', 'inet6 addr:fe80::210:f3ff:fe6d:d2d1/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:11 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:638 (638.0 b)', 'Vsys:root_vsys  Ifindex:94', 'Commtype:routing', 'feth33    Description:金宏网', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:d2', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:35.1.70.5  Bcast:35.1.70.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6d:d2d2/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:1449662788 Tx packets:478892968 Dropped:132352', 'RX bytes:1748728779019 (1628.6 Gb)  TX bytes:251228547713 (233.9 Gb)', 'Vsys:root_vsys  Ifindex:95', 'Commtype:routing', 'feth34    Description:金宏-独立网段', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:cf', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:10.152.0.1  Bcast:10.152.0.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6d:d2cf/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:2454147 Tx packets:324493 Dropped:1273892', 'RX bytes:295562935 (281.8 Mb)  TX bytes:300434616 (286.5 Mb)', 'Vsys:root_vsys  Ifindex:92', 'Commtype:routing', 'feth35    Description:教育', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:d0', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:10.1.1.2  Bcast:10.1.1.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6d:d2d0/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:0 Tx packets:15 Dropped:0', 'RX bytes:0 (0.0 b)  TX bytes:806 (806.0 b)', 'Vsys:root_vsys  Ifindex:93', 'Commtype:routing', 'feth36    Description:青岛银行及建行', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:cd', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:172.31.16.2  Bcast:172.31.16.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6d:d2cd/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:626328 Tx packets:805 Dropped:2', 'RX bytes:73670664 (70.2 Mb)  TX bytes:45218 (44.1 kb)', 'Vsys:root_vsys  Ifindex:90', 'Commtype:routing', 'feth37    Description:其他商业银行', 'Link encap:Ethernet  HWaddr 00:10:f3:6d:d2:ce', 'Link status: established, Autoneg enable', 'Full-duplex, 1000Mb/s', 'inet addr:179.59.195.130  Bcast:179.59.195.255  Mask:255.255.255.0', 'inet6 addr:fe80::210:f3ff:fe6d:d2ce/64  Scope:Link', 'UP BROADCAST RUNNING MULTICAST  MTU:1500', 'Rx packets:12872367 Tx packets:16473902 Dropped:74448', 'RX bytes:2762606869 (2.5 Gb)  TX bytes:18002842906 (16.7 Gb)', 'Vsys:root_vsys  Ifindex:91', 'Commtype:routing'], 'snmp': ['snmp show status', 'error -100110 : 无效输入，分析错误'], 'mactable': ['network mac-address-table show', 'error -100110 : 无效输入，分析错误'], 'policynum': ['firewall policy total', 'total:17'], 'natnum': ['nat policy total', 'error -100110 : 无效输入，分析错误'], 'lognum': ['log log count type mix', 'error -100110 : 无效输入，分析错误'], 'devname': ['system devname show', 'system devname set TopsecOS'], 'webui': ['system webui show', 'system webui idle-timeout 3600'], 'servicestatus': ['system service status', 'system service telnetd on', 'system service sshd on', 'system service httpd on'], 'localservice': ['pf service show', 'ID 17898 pf service add name ssh area area_feth0 addressname any', 'ID 17899 pf service add name telnet area area_feth0 addressname any', 'ID 17900 pf service add name webui area area_feth0 addressname 192.168.1.0', 'ID 17901 pf service add name ping area area_feth0 addressname any', 'ID 17902 pf service add name webui area 内网区域 addressname 10.28.30.0', 'ID 17903 pf service add name ssh area 内网区域 addressname any', 'ID 17904 pf service add name ping area 人行区域 addressname any', 'ID 17905 pf service add name ping area 专员区域 addressname any', 'ID 17906 pf service add name ping area 金宏区域 addressname any', 'ID 17907 pf service add name ping area 金宏独立网段区域 addressname any', 'ID 17908 pf service add name ping area 教育区域 addressname any', 'ID 17909 pf service add name ping area 青岛银行及建行区域 addressname any', 'ID 17910 pf service add name ping area 商业银行区域 addressname any', 'ID 17911 pf service add name ping area 内网区域 addressname any', 'ID 18399 pf service add name snmp area 内网区域 addressname snmp', 'ID 19119 pf service add name webui area 内网区域 addressname snmp', 'ID 20528 pf service add name telnet area 内网区域 addressname any'], 'ALG': ['dpi policy show', 'error -100110 : 无效输入，分析错误'], 'session': ['network session show configuration', 'network session timeout deny 10', 'network session timeout close 20', 'network session timeout other 20', 'network session timeout handshake 20', 'network session timeout udp 60', 'network session timeout established 1800', 'network session timeout never-expire 86400', 'network session timeout ha-expire 7200', 'network session not_same_cpu_drop on', 'network session timer delete 512', 'network session reclaim delete 256', 'network session defrag on', 'network session close_timer timeout 512 del_nums 64', 'network session tcp-reset off', 'network session session-integrity on', 'network session only-syn-create on', 'network session packet-checksum off', 'network session syn-reset off', 'network session reclaim all', 'network session never-expire-percent 10'], 'area': ['define area show', "ID 20829 define area add name area_feth0 interface 'feth0 ' refered 4", "ID 20830 define area add name 内网区域 interface 'feth10 feth30 ' comment ' ' re", 'fered 33', "ID 20831 define area add name 人行区域 interface 'feth31 ' comment ' ' refered 2", '7', "ID 20832 define area add name 专员区域 interface 'feth32 ' comment ' ' refered 6", "ID 20833 define area add name 金宏区域 interface 'feth33 ' comment ' ' refered 3", '2', "ID 20834 define area add name 金宏独立网段区域 interface 'feth34 ' comment ' ' r", 'efered 5', "ID 20835 define area add name 教育区域 interface 'feth35 ' comment ' ' refered 9", "ID 20836 define area add name 青岛银行及建行区域 interface 'feth36 ' comment ' '", 'refered 16', "ID 20837 define area add name 商业银行区域 interface 'feth37 ' comment ' ' refer", 'ed 55'], 'ha': ['ha show configuration', 'error -100110 : 无效输入，分析错误', 'ha show status', 'HA-Status: ha enable', 'Heartbeat-Link: feth1,  established', 'Heartbeat-Local IP: 100.100.100.1', 'Heartbeat-Remote IP: 100.100.100.2', 'Group 1', 'State       Preempt    Priority   Interface', 'ACTIVE      disable    65000      feth10,feth31,feth32,feth33,feth34,feth35,feth', '36,feth37'], 'log': ['log log set_show', 'error -100110 : 无效输入，分析错误'], 'authpolicy': ['system authset config show', 'error -100110 : 无效输入，分析错误']}

    #
    #outputDic={ 'customname': 'test', 'type': 'fwtos020', 'hostname': 'COM5', 'port': 9600, 'username': 'superman', 'password': 'Talent@232021', 'login_info': b'TopsecOS# ', 'isfirst': '1', 'model': ['system product show\r\n', 'TopSDWAN(SDW-11106-CPELW-S)\r\n', ' \r\n'], 'version': ['system version\r\n', 'Topsec Operating System\r\n', '\r\n', 'v3.3.030.027.87_B5685_MORE\r\n', '\r\n', 'Q2104039153\r\n', ' \r\n'], 'time': ['system time show\r\n', '+08 2022-04-09 20:31:53\r\n', ' \r\n'], 'ntp': ['system ntp show\r\n', 'Ntp stopped \r\n', ' \r\n'], 'uptime': ['system uptime\r\n', 'UP 01:48\r\n', ' \r\n'], 'sysinfo': ['system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            1.0%\r\n', '    System          1.4%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      2.8%\r\n', '    Idle            97.2%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.2%\r\n', '    System          0.4%\r\n', '    Nice            0.0%\r\n', '    Si              0.2%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.9%\r\n', '    Idle            99.1%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.4%\r\n', '    System          1.1%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      1.9%\r\n', '    Idle            98.1%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.4%\r\n', '    System          0.2%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      1.0%\r\n', '    Idle            99.0%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            1.1%\r\n', '    System          2.8%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.2%\r\n', '    Iowait          0.0%\r\n', '    Total Used      4.4%\r\n', '    Idle            95.6%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.2%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.2%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.5%\r\n', '    Idle            99.5%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.0%\r\n', '    System          0.2%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.6%\r\n', '    Idle            99.4%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.2%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.3%\r\n', '    Idle            99.7%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.2%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.2%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.5%\r\n', '    Idle            99.5%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n', 'system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.2%\r\n', '    System          1.1%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      1.7%\r\n', '    Idle            98.3%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n'], 'meminfo': ['system information\r\n', 'Physical Memory Information:\r\n', '  Total            3872112 KB\r\n', "  Free's ratio     80.4%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.5%\r\n', '    System          0.2%\r\n', '    Nice            0.0%\r\n', '    Si              0.3%\r\n', '    Hi              0.1%\r\n', '    Iowait          0.0%\r\n', '    Total Used      1.1%\r\n', '    Idle            98.9%\r\n', '  cpu1:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Iowait          0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 0\r\n', '  Speed(cps)= 0\r\n', 'v6_Session Numbers Information:\r\n', '  Total= 0\r\n', '  Speed(cps)= 0\r\n', ' \r\n'], 'interface': ['network interface show\r\n', 'eth0      Description:intranet\r\n', '          Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:68  \r\n', '          Link status:not established, Autoneg enable\r\n', '          inet addr:192.168.1.254  Bcast:192.168.1.255  Mask:255.255.255.0\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1ae4000-1ae4fff \r\n', '\r\n', 'eth1      Description:internet\r\n', '          Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:69  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1ae6000-1ae6fff \r\n', '\r\n', 'eth2      Description:ssn\r\n', '          Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:66  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1ae0000-1ae0fff \r\n', '\r\n', 'eth3      Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:67  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1ae2000-1ae2fff \r\n', '\r\n', 'eth4      Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:6A  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1ae8000-1ae8fff \r\n', '\r\n', 'eth5      Link encap:Ethernet  HWaddr 68:91:D0:D9:0D:6B  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '          Memory:1aea000-1aeafff \r\n', '\r\n', 'wlan0     Link encap:Ethernet  HWaddr B8:B7:F1:07:8C:24  \r\n', '          Link status: established, Autoneg disable\r\n', '          Unknown-duplex, unknown speed\r\n', '          inet addr:172.18.1.254  Bcast:172.18.1.255  Mask:255.255.255.0\r\n', '          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:76 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:84 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:8498 (8.29 kb)  TX bytes:11002 (10.74 kb)\r\n', '\r\n', 'lte0      Link encap:Ethernet  HWaddr DE:58:2C:40:FA:09  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '\r\n', 'wlan1     Link encap:Ethernet  HWaddr C8:02:8F:00:42:92  \r\n', '          Link status: established, Autoneg disable\r\n', '          Unknown-duplex, unknown speed\r\n', '          inet addr:172.18.2.254  Bcast:172.18.2.255  Mask:255.255.255.0\r\n', '          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1\r\n', '          RX packets:60 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:69 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:6036 (5.89 kb)  TX bytes:8168 (7.97 kb)\r\n', '\r\n', 'ipsec0    Link encap:UNSPEC  HWaddr 00-00-00-00-00-00  \r\n', '          NOARP  MTU:0  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:10 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '\r\n', 'ipsec1    Link encap:UNSPEC  HWaddr 00-00-00-00-00-00  \r\n', '          NOARP  MTU:0  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:10 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '\r\n', 'ipsec2    Link encap:UNSPEC  HWaddr 00-00-00-00-00-00  \r\n', '          NOARP  MTU:0  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:10 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '\r\n', 'ipsec3    Link encap:UNSPEC  HWaddr 00-00-00-00-00-00  \r\n', '          NOARP  MTU:0  Metric:1\r\n', '          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:10 \r\n', '          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)\r\n', '\r\n', ' \r\n'], 'snmp': ['snmp show status\r\n', 'snmpd is not running!\r\n', ' \r\n'], 'mactable': ['network mac-address-table show\r\n', 'Total:8192 static:0 link:4 pool:8187 route:4\r\n', '0 matched mac entries.\r\n', '\r\n', ' \r\n'], 'policynum': ['firewall policy total\r\n', 'total: 0\r\n', ' \r\n'], 'natnum': ['nat policy total\r\n', 'total: 0\r\n', ' \r\n'], 'lognum': ['log log count type mix\r\n', 'Total log : 0 \r\n', ' \r\n'], 'devname': ['system devname show\r\n', 'system devname set TopsecOS\r\n', ' \r\n'], 'webui': ['system webui show\r\n', 'system webui idle-timeout 180\r\n', 'system webui ssl-verify-client no\r\n', 'system webui max-client 10\r\n', 'system webui admin-port 443\r\n', 'system webui language chinese\r\n', ' \r\n'], 'servicestatus': ['system service status\r\n', '\r\n', 'system sshd stop\r\n', 'system telnetd stop\r\n', 'system httpd start\r\n', 'system monitord start\r\n', '\r\n', ' \r\n'], 'localservice': ['pf service show\r\n', 'pf service log off\r\n', 'ID 8013 pf service add name ping area area_eth0 addressname any \r\n', 'ID 8014 pf service add name webui area area_eth0 addressname any \r\n', 'ID 8062 pf service add name dhcp area area_eth0 addressname any \r\n', 'ID 8066 pf service add name webui area area_wlan addressname any \r\n', 'ID 8067 pf service add name dhcp area area_wlan addressname any \r\n', ' \r\n'], 'ALG': ['dpi policy show\r\n', 'ID 8019 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 21 name ftp en\r\n', 'able yes\r\n', 'ID 8020 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 25 name smtp e\r\n', 'nable yes\r\n', 'ID 8021 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol udp port 69 name tftp e\r\n', 'nable yes\r\n', 'ID 8022 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 80 name http e\r\n', 'nable yes\r\n', 'ID 8023 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 110 name pop3 \r\n', 'enable yes\r\n', 'ID 8024 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol udp port 53 name dns en\r\n', 'able yes\r\n', '\r\n', ' \r\n'], 'session': ['network session show configuration\r\n', 'network session timeout default\r\n', 'network session never-expire-time 604800\r\n', 'network session protocol default\r\n', 'network session icmp-redirect off\r\n', 'network session tcp-reset off\r\n', 'network session session-integrity on\r\n', 'network session only-syn-create on\r\n', 'network session packet-checksum off\r\n', 'network session syn-reset off\r\n', 'network session log-op delete on\r\n', 'network session log-op create off\r\n', 'network session log-op statistics off\r\n', 'network session quota tcp 0\r\n', 'network session quota udp 0\r\n', 'network session quota other 0\r\n', 'network session count on\r\n', 'network session count interval 5\r\n', ' \r\n'], 'area': ['define area show\r\n', "ID 8002 define area add name area_eth0 attribute 'eth0 ' access on vsid 0 refere\r\n", 'd 3 \r\n', "ID 8065 define area add name area_wlan attribute 'wlan0 wlan1 ' access on vsid 0\r\n", ' refered 2 \r\n', ' \r\n'], 'ha': ['ha show configuration\r\n', '\r\n', 'ha clean\r\n', 'ha mode as\r\n', 'ha as-vrid 100\r\n', 'ha gratuitous-arp enable\r\n', 'ha hello-interval 1\r\n', 'ha ip-probe disable\r\n', 'ha rtosync enable\r\n', 'ha rtosync fast disable\r\n', 'ha disable\r\n', ' \r\n', 'ha show status\r\n', 'Ha-Status: ha disable\r\n', ' \r\n'], 'log': ['log log set_show\r\n', "log server ipaddr '192.168.1.253'\r\n", 'log server port 514\r\n', 'log transfer protocol UDP\r\n', 'log logtype syslog\r\n', 'log transmit option disable\r\n', 'log trans_gather option yes\r\n', 'log language english\r\n', 'log crypt option disable\r\n', 'log key: none\r\n', 'log type: none\r\n', 'log level: 0\r\n', 'log local save: disable\r\n', ' \r\n'], 'authpolicy': ['system authset config show\r\n', 'error -8010 : 无效输入，分析错误\r\n']}

    # print(json.dumps(outputDic,indent=4))
    area=fwtos020_area(outputDic['area'])
    model = fwtos020_model(outputDic['model'])
    (ver, psn) = fwtos020_version(outputDic['version'])  # 已测试
    uptime = fwtos020_uptime(outputDic['uptime'])
    ha = fwtos020_ha(outputDic['ha'])
    # ng23_area(outputDic['area'])   #ng系统中检查此项无意义
    # ng23_sessionconfig(outputDic['sessionconfig']) #与session()重复
    intergrityValue = fwtos020_session(outputDic['session'])
    ALG = fwtos020_ALG(outputDic['ALG'])
    name = fwtos020_devname(outputDic['devname'])
    #dhcp = fwtos020_dhcp(outputDic['dhcp'])
    interface = fwtos020_interface(outputDic['interface'])
    localservice = fwtos020_localservice(outputDic['localservice'])
    log = fwtos020_log(outputDic['log'])
    lognum = fwtos020_lognum(outputDic['lognum'])
    nat = fwtos020_natnum(outputDic['natnum'])
    policy = fwtos020_policynum(outputDic['policynum'])
    snmp = fwtos020_snmp(outputDic['snmp'])

    cpuavg = fwtos020_sysinfo(outputDic['sysinfo'])
    mem = fwtos020_meminfo(outputDic['meminfo'])
    webui = fwtos020_webui(outputDic['webui'])
    ntp = fwtos020_ntp(outputDic['ntp'])
    servicestatus = fwtos020_servicestatus(outputDic['servicestatus'])
    authpolicy = fwtos020_authpolicy(outputDic['authpolicy'])
    mactable = fwtos020_mactable(outputDic['mactable'])
    print(
        area,
        model,
        ntp,
        ver,
        psn,
        uptime,
        cpuavg,
        mem,
        interface,
        snmp,

        mactable,
        policy,
        nat,
        lognum,
        name,
        webui,
        servicestatus,
        localservice,
        ALG,
        intergrityValue,
        ha,
        log,
        authpolicy)


"""

 {'self': <__main__.sshDevice object at 0x0000023FE55BFBB0>, 'hostname': '192.168.1.254', 'port': '22', 'username': 'superman', 'password': 'Talent@123', 'customname': 'a', 'version': ['system version\r\n', 'Topsec Operating System\r\n', '\r\n', 'tos_3.3.017.048.1_D8551_4_B3015_more\r\n', '\r\n', 'Q2101000279\r\n', ' \r\n'], 'uptime': ['system uptime\r\n', 'UP 01:53\r\n', ' \r\n'], 'time': ['system time show\r\n', '+08 2021-10-21 23:22:33\r\n', ' \r\n'], 'ntp': ['system ntp show\r\n', 'Ntp stopped \r\n', ' \r\n'], 'sysinfo': ['system information\r\n', 'Physical Memory Information:\r\n', '  Total            1735500 KB\r\n', "  Free's ratio     87.5%\r\n", 'CPU Load Information:(2 cpus)\r\n', '  cpu0:\r\n', '    User            0.0%\r\n', '    System          0.0%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Total Used      0.0%\r\n', '    Idle            100.0%\r\n', '  cpu(fast)1:\r\n', '    User            0.0%\r\n', '    System          0.1%\r\n', '    Nice            0.0%\r\n', '    Si              0.0%\r\n', '    Hi              0.0%\r\n', '    Total Used      0.1%\r\n', '    Idle            99.9%\r\n', 'Session Numbers Information:\r\n', '  Totalnum= 6\r\n', '  Speed(cps)= 0\r\n', ' \r\n'], 'snmpcfg': ['snmp show config\r\n', 'snmp set contact <support@topsec.com.cn> \r\n', 'snmp set location www.topsec.com.cn \r\n', ' \r\n'], 'snmp': ['snmp show status\r\n', 'snmpd is not running!\r\n', ' \r\n'], 'interface': ['network interface show\r\n', 'eth0      Description:intranet\r\n', '          Link encap:Ethernet  HWaddr 00:0D:48:44:E4:FC  \r\n', '          Link status: established, Autoneg enable\r\n', '          Full-duplex, 1000Mb/s\r\n', '          inet addr:192.168.1.254  Bcast:192.168.1.255  Mask:255.255.255.0\r\n', '          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1 LPE:off\r\n', '          RX packets:11116 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:12334 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:2376006 (2.2 Mb)  TX bytes:10691689 (10.1 Mb)\r\n', '          Interrupt:16 \r\n', '\r\n', 'eth1      Description:internet\r\n', '          Link encap:Ethernet  HWaddr 00:0D:48:44:E4:FD  \r\n', '          Link status:not established, Autoneg enable\r\n', '          UP BROADCAST MULTICAST  MTU:1500  Metric:1 LPE:off\r\n', '          RX packets:929 errors:0 dropped:0 overruns:0 frame:0\r\n', '          TX packets:2337 errors:0 dropped:0 overruns:0 carrier:0\r\n', '          collisions:0 txqueuelen:100 \r\n', '          RX bytes:147370 (143.9 kb)  TX bytes:735924 (718.6 kb)\r\n', '          Interrupt:17 \r\n', '\r\n', ' \r\n'], 'session': ['network session show configuration\r\n', 'network session timeout default\r\n', 'network session protocol default\r\n', 'network session icmp-redirect off\r\n', 'network session tcp-reset off\r\n', 'network session session-integrity on\r\n', 'network session only-syn-create on\r\n', 'network session packet-checksum off\r\n', 'network session syn-reset off\r\n', 'network session log-op delete on\r\n', 'network session log-op create off\r\n', 'network session log-op statistics off\r\n', 'network session quota tcp 0\r\n', 'network session quota udp 0\r\n', 'network session quota other 0\r\n', 'network session count off\r\n', 'network session count interval 5\r\n', ' \r\n'], 'dhcp': ['network dhcp show status\r\n'], 'policynum': ['firewall policy total\r\n', 'total: 0\r\n', ' \r\n'], 'natnum': ['nat policy total\r\n', 'total: 0\r\n', ' \r\n'], 'log': ['log log set_show\r\n', "log server ipaddr '192.168.1.253'\r\n", 'log server port 514\r\n', 'log transfer protocol UDP\r\n', 'log logtype syslog\r\n', 'log transmit option disable\r\n', 'log trans_gather option yes\r\n', 'log crypt option disable\r\n', 'log key: \r\n', 'log type: none\r\n', 'log level: 0\r\n', ' \r\n'], 'devname': ['system devname show\r\n', 'system devname set TopsecOS\r\n', ' \r\n'], 'webui': ['system webui show\r\n', 'system webui idle-timeout 180\r\n', 'system webui ssl-verify-client no\r\n', 'system webui max-client 10\r\n', 'system webui admin-port 443\r\n', ' \r\n'], 'servicestatus': ['system service status\r\n', '\r\n', 'system sshd start\r\n', 'system telnetd stop\r\n', 'system httpd start\r\n', 'system monitord start\r\n', '\r\n', ' \r\n'], 'localservice': ['pf service show\r\n', 'pf service log off\r\n', 'ID 8013 pf service add name ping area area_eth0 addressname any \r\n', 'ID 8014 pf service add name webui area area_eth0 addressname any \r\n', 'ID 8058 pf service add name ssh area area_eth0 addressname any \r\n', ' \r\n'], 'mactable': ['network mac-address-table show\r\n', 'Total:8192 static:0 link:1 pool:8190 route:1\r\n', '0 matched mac entries.\r\n', '\r\n', ' \r\n'], 'ALG': ['dpi policy show\r\n', 'ID 8017 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 21 name ftp enable yes\r\n', 'ID 8018 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 25 name smtp enable yes\r\n', 'ID 8019 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol udp port 69 name tftp enable yes\r\n', 'ID 8020 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 80 name http enable yes\r\n', 'ID 8021 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 110 name pop3 enable yes\r\n', 'ID 8022 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 1521 name sqlnet enable yes\r\n', 'ID 8023 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 143 name imap enable yes\r\n', 'ID 8024 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol tcp port 23 name telnet enable yes\r\n', 'ID 8025 dpi policy add net 0.0.0.0 mask 0.0.0.0 protocol udp port 53 name dns enable yes\r\n', '\r\n', ' \r\n'], 'ha': ['ha show status\r\n', 'Ha-Status: ha disable\r\n', ' \r\n'], 'model': ['system product show\r\n', 'NGFW4000-UF(TG-51130)\r\n', ' \r\n'], 'authpolicy': ['system authset config show\r\n', 'system authset authfail set maxnum 5 \r\n', 'system authset usermaxlogin set maxnum 10 \r\n', 'system authset maxonlineadm set maxnum 5 \r\n', 'system authset managermaxlogin set maxnum 5 \r\n', 'system authset faillock set time 60 \r\n', 'system authset passwd-type set type plain\r\n', 'system authset timeout set num 100\r\n', 'system authset get-lostinfo set type disable\r\n', 'system authset passwd-policy set-user policy weak first-login no min-length 6 \r\n', 'system authset passwd-policy set-manager policy strong min-length 8 \r\n', 'system authset secpolicy-set set before on after off after-timer 0\r\n', 'system authset manager-login-number set status off\r\n', ' \r\n']}

"""
