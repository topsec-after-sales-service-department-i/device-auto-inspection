#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: docx
# @Function:
# @Author: RyneZ
# @Time: 2021/4/15 14:34
# from docx import Document
# from docx.shared import Cm
# from docx.shared import Cm

import os
import time as timeModule
from docx import Document
from docx.opc.exceptions import *
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.shared import Pt,RGBColor


def fwng23_outdoc(docxtempfile, valuelist, customname, hostip):

    def version(table, value):
        table.rows[1].cells[5].text = value[0]
        table.rows[2].cells[2].text = value[1]

    def model(table, value):
        table.rows[1].cells[2].text = value

    def time(table, value):
        table.rows[16].cells[5].text = value[0]
        table.rows[2].cells[5].text = value[1]

    def uptime(table, value):
        table.rows[15].cells[5].text = str(value) + '天'

    def logstat(table, value):
        string = '日志droped数：' + value[0] + '\nfailed数量：' + value[1]
        table.rows[19].cells[5].text = string

    def ntp(table, value):
        table.rows[22].cells[5].text = 'ntp服务状态：' + \
            value[1] + '\n配置的同步地址为：' + value[0]

    def sysinfo(table, value):
        table.rows[11].cells[5].text = str(value) + '%'

    def meminfo(table, value):
        table.rows[12].cells[5].text = '硬盘占用：' + \
            value[0] + '\n内存占用：' + value[1]

    def interface(table, value):
        string = ''
        for interface, packets in value.items():
            if interface == 'feth999':
                continue
            rxpackets = packets[0].split()[1]
            rxerrors = packets[0].split()[2]
            rxdrops = packets[0].split()[3]
            txpackets = packets[1].split()[1]
            txerrors = packets[1].split()[2]
            string = string + '接口' + interface + '收到' + rxpackets + '个包，丢弃：' + rxdrops + \
                '个，错误包：' + rxerrors + '个;共发出' + txpackets + '个包，错误包' + txerrors + '个\n'
        table.rows[13].cells[5].text = string.strip()

    def snmp(table, value):
        table.rows[31].cells[5].text = value.strip()

    # def dhcp(table, value):
    #     table.rows[20].cells[5].text = value.strip()

    def policynum(table, value):
        table.rows[17].cells[5].text = str(value)

    def natnum(table, value):
        table.rows[18].cells[5].text = str(value)

    def devname(table, value):
        table.rows[14].cells[5].text = value

    def webui(table, value):
        table.rows[23].cells[5].text = str(value)

    def servicestatus(table, value):
        string = ''
        for service, status in value.items():
            string = string + service + '状态:' + status + '\n'
        table.rows[24].cells[5].text = string.strip()

    def localservice(table, value):
        string = ''
        for service, addresses in value.items():
            addressstr = ''
            # 将第一个元素相同的列表合并，即将同一个区域的限制地址合并到一个列表中
            newaddrdict = {}
            for key, value in addresses:
                if key not in newaddrdict.keys():
                    newaddrdict[key] = [key]
                newaddrdict[key].append(value)
            results = list(newaddrdict.values())
            for address in results:
                if address[0]:
                    addressstr = addressstr + '\n限制区域：' + address[0] + ';限制地址：' + ','.join(address[1:])
                else:
                    addressstr = addressstr + '\n限制区域为空;限制地址：' + ','.join(address[1:])
            string = string + service + ':' + addressstr + '\n'
        table.rows[25].cells[5].text = string.strip()

    def ALG(table, value):
        string = ''
        for alg, status in value.items():
            string = string + alg + ':' + status + '\n'
        table.rows[26].cells[5].text = string.strip()

    def session(table, value):
        string = '连接完整性：' + value[0] + '\n已建立tcp连接超时时间:' + value[5] + '\n已握手连接超时时间:' + value[3] + \
            '\n已断开连接超时时间:' + value[1] + '\n其他tcp连接超时时间' + value[2] + '\nUDP连接超时时间:' + value[4]
        table.rows[27].cells[5].text = string.strip()

    def authpolicy(table, value):
        string = '复杂度：' + value[0] + '\n密码最小长度：' + value[1] + \
            '\n允许登录失败次数：' + value[2] + '\n账号锁定时间：' + value[3]
        table.rows[29].cells[5].text = string

    def ha(table, value):
        setstr = ''
        if value[0] == 'ha disable':
            setstr = '未开启'
            table.rows[30].cells[5].text = setstr
            return 0
        if value[1] == 'sp':
            setstr = '当前运行为连接保护模式'
        elif value[1] == 'aa':
            setstr = '当前运行为负载均衡模式，需手动登陆设备检查'
        elif value[1] == 'as':
            curstat = value[2].split()
            setstr = '当前运行为双机热备模式\n'
            try:
                setstr = setstr + '运行状态：' + curstat[0] + '\n是否抢占：' + curstat[1] +'\n优先级：'+curstat[2]+ '\n监控接口：' + curstat[3]
            except BaseException:
                setstr = setstr + '运行状态：' + curstat[0] + '\n是否抢占：' + curstat[1] +'\n优先级：'+curstat[2]+  '\n监控接口：未配置'

        table.rows[30].cells[5].text = setstr

    def log(table, value):
        serverlist = ''
        logsetstr = ''
        for item in value[0]:
            serverlist = serverlist + item + ','

        logsetstr = '日志服务器地址:' + \
            serverlist + '\n是否外发:' + value[1] + '\n是否存储本地:' + value[2] + '\n'
        typedic = {
            'dualpower_monitor': '双路电源监控日志',
            'abnormal_threat': '行为分析',
            'ac': '访问控制',
            'admin': '管理员日志',
            'admin_config': '操作管理员日志',
            'ads_clean': 'ADS',
            'apt': '高级可持续性攻击防御',
            'cpu_monitor': 'CPU占用率',
            'cputemp_monitor': 'CPU温度日志',
            'ctrlsess': '连接限制',
            'data_filter': '数据过滤',
            'disk_monitor': '磁盘占用率',
            'file_block': '文件过滤',
            'firmware_update': '固件升级',
            'ha': '高可用性',
            'ids': 'IDS联动',
            'interface': '接口链路状态',
            'ipmac': 'ip mac绑定',
            'ips': '入侵防御',
            'license_update': 'license升级',
            'linkbak': '链路备份',
            'memory_monitor': '内存占用率',
            'mgmt': '管理员配置',
            'nat': '网络地址转换',
            'neighbour': 'ipv6地址冲突检测',
            'pf': '本机服务',
            'pf_rule': '黑名单',
            'recover': '服务监控',
            'rules_update': '规则库升级',
            'session': '连接',
            'spam': '邮件安全',
            'streamav': '病毒过滤',
            'systemp_monitor': '系统温度日志',
            'topguard': '云检测',
            'url_filter': 'URL过滤',
            'user_auth': '用户认证',
            'vpn': 'ipsecvpn'}
        leveldic = {
            '0': '紧急',
            '1': '告警',
            '2': '严重',
            '3': '错误',
            '4': '警示',
            '5': '通知',
            '6': '信息',
            '7': '调试',
            '8': '未开启'}
        for typename, level in value[3].items():
            if typename in ['ac', 'mgmt', 'interface', 'ha']:
                logsetstr = logsetstr + \
                    typedic[typename] + ':' + leveldic[level] + '\n'
        table.rows[28].cells[5].text = logsetstr.strip()

    document = Document(docxtempfile)
    table = document.tables[0]  # 处理doc文件中的第一个表格
    dirstr = './' + customname  # 输出目录为客户名称
    for comlist, value in valuelist.items():
        try:
            eval(comlist)(table, value)
        except IndexError:
            print('从返回数据列表取值填入表格时出错，当前处理的数据为：' + str(value))
            continue
        except KeyError:
            print('暂时不能处理该项，当前处理的数据为：' + comlist + '+' + str(value))
            continue
    table.rows[0].cells[2].text = customname
    # 统一居中
    for i in range(0, 3):
        table.rows[i].cells[2].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(11, 19):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(22, 32):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    # 同一设置字体大小为小五
    for row in table.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.size = Pt(9)
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    try:
        os.makedirs(dirstr)
    except FileExistsError:
        pass
    except PackageNotFoundError:
        print('模板未找到，请查看是否存在以下文件：' + docxtempfile)
        exit(-1)
    try:
        document.save(customname + "/" + hostip+ timeModule.strftime('-%m%d%H%M%S', timeModule.localtime()) + ".docx")
    except FileNotFoundError:
        print('未找到输出目录./' + customname + '/')


def fwng26_outdoc(docxtempfile, valuelist, customname, hostip):

    def version(table, value):
        table.rows[1].cells[5].text = value[0]
        table.rows[2].cells[2].text = value[1]

    def model(table, value):
        table.rows[1].cells[2].text = value

    def time(table, value):
        table.rows[16].cells[5].text = value[0]
        table.rows[2].cells[5].text = value[1]

    def uptime(table, value):
        table.rows[15].cells[5].text = str(value) + '天'

    def logstat(table, value):
        string = '日志droped数：' + value[0] + '\nfailed数量：' + value[1]
        table.rows[19].cells[5].text = string

    def ntp(table, value):
        table.rows[22].cells[5].text = 'ntp服务状态：' + \
            value[1] + '\n配置的同步地址为：' + value[0]

    def sysinfo(table, value):
        table.rows[11].cells[5].text = str(value) + '%'

    def meminfo(table, value):
        table.rows[12].cells[5].text = '硬盘占用：' + \
            value[0] + '\n内存占用：' + value[1]

    def interface(table, value):
        string = ''
        for interface, packets in value.items():
            if interface == 'feth999':
                continue
            rxpackets = packets[0].split()[1]
            rxerrors = packets[0].split()[2]
            rxdrops = packets[0].split()[3]
            txpackets = packets[1].split()[1]
            txerrors = packets[1].split()[2]
            string = string + '接口' + interface + '收到' + rxpackets + '个包，丢弃：' + rxdrops + \
                '个，错误包：' + rxerrors + '个;共发出' + txpackets + '个包，错误包' + txerrors + '个\n'
        table.rows[13].cells[5].text = string.strip()

    def snmp(table, value):
        table.rows[31].cells[5].text = value.strip()

    # def dhcp(table, value):
    #     table.rows[20].cells[5].text = value.strip()

    def policynum(table, value):
        table.rows[17].cells[5].text = str(value)

    def natnum(table, value):
        table.rows[18].cells[5].text = str(value)

    def devname(table, value):
        table.rows[14].cells[5].text = value

    def webui(table, value):
        table.rows[23].cells[5].text = str(value)

    def servicestatus(table, value):
        string = ''
        for service, status in value.items():
            string = string + service + '状态:' + status + '\n'
        table.rows[24].cells[5].text = string.strip()

    def localservice(table, value):
        string = ''
        for service, addresses in value.items():
            addressstr = ''
            # 将第一个元素相同的列表合并，即将同一个区域的限制地址合并到一个列表中
            newaddrdict = {}
            for key, value in addresses:
                if key not in newaddrdict.keys():
                    newaddrdict[key] = [key]
                newaddrdict[key].append(value)
            results = list(newaddrdict.values())
            for address in results:
                if address[0]:
                    addressstr = addressstr + '\n限制区域：' + address[0] + ';限制地址：' + ','.join(address[1:])
                else:
                    addressstr = addressstr + '\n限制区域为空;限制地址：' + ','.join(address[1:])
            string = string + service + ':' + addressstr + '\n'
        table.rows[25].cells[5].text = string.strip()

    def ALG(table, value):
        string = ''
        for alg, status in value.items():
            string = string + alg + ':' + status + '\n'
        table.rows[26].cells[5].text = string.strip()

    def session(table, value):
        string = '连接完整性：' + value[0] + '\n已建立tcp连接超时时间:' + value[5] + '\n已握手连接超时时间:' + value[3] + \
            '\n已断开连接超时时间:' + value[1] + '\n其他tcp连接超时时间' + value[2] + '\nUDP连接超时时间:' + value[4]
        table.rows[27].cells[5].text = string.strip()

    def authpolicy(table, value):
        string = '复杂度：' + value[0] + '\n密码最小长度：' + value[1] + \
            '\n允许登录失败次数：' + value[2] + '\n账号锁定时间：' + value[3]
        table.rows[29].cells[5].text = string

    def ha(table, value):
        setstr = ''
        if value[0] == 'ha disable':
            setstr = '未开启'
            table.rows[30].cells[5].text = setstr
            return 0
        if value[1] == 'sp':
            setstr = '当前运行为连接保护模式'
        elif value[1] == 'aa':
            setstr = '当前运行为负载均衡模式，需手动登陆设备检查'
        elif value[1] == 'as':
            curstat = value[2].split()
            setstr = '当前运行为双机热备模式\n'
            try:
                setstr = setstr + '运行状态：' + curstat[0] + '\n是否抢占：' + curstat[1] + '\n优先级：' + curstat[2] + '\n监控接口：' + \
                         curstat[3]
            except BaseException:
                setstr = setstr + '运行状态：' + curstat[0] + '\n是否抢占：' + curstat[1] + '\n优先级：' + curstat[2] + '\n监控接口：未配置'

        table.rows[30].cells[5].text = setstr

    def log(table, value):
        serverlist = ''
        logsetstr = ''
        for item in value[0]:
            serverlist = serverlist + item + ','

        logsetstr = '日志服务器地址:' + \
            serverlist + '\n是否外发:' + value[1] + '\n是否存储本地:' + value[2] + '\n'
        typedic = {
            'dualpower_monitor': '双路电源监控日志',
            'abnormal_threat': '行为分析',
            'ac': '访问控制',
            'admin': '管理员日志',
            'admin_config': '操作管理员日志',
            'ads_clean': 'ADS',
            'apt': '高级可持续性攻击防御',
            'cpu_monitor': 'CPU占用率',
            'cputemp_monitor': 'CPU温度日志',
            'ctrlsess': '连接限制',
            'data_filter': '数据过滤',
            'disk_monitor': '磁盘占用率',
            'file_block': '文件过滤',
            'firmware_update': '固件升级',
            'ha': '高可用性',
            'ids': 'IDS联动',
            'interface': '接口链路状态',
            'ipmac': 'ip mac绑定',
            'ips': '入侵防御',
            'license_update': 'license升级',
            'linkbak': '链路备份',
            'memory_monitor': '内存占用率',
            'mgmt': '管理员配置',
            'nat': '网络地址转换',
            'neighbour': 'ipv6地址冲突检测',
            'pf': '本机服务',
            'pf_rule': '黑名单',
            'recover': '服务监控',
            'rules_update': '规则库升级',
            'session': '连接',
            'spam': '邮件安全',
            'streamav': '病毒过滤',
            'systemp_monitor': '系统温度日志',
            'topguard': '云检测',
            'url_filter': 'URL过滤',
            'user_auth': '用户认证',
            'vpn': 'ipsecvpn'}
        leveldic = {
            '0': '紧急',
            '1': '告警',
            '2': '严重',
            '3': '错误',
            '4': '警示',
            '5': '通知',
            '6': '信息',
            '7': '调试',
            '8': '未开启'}
        for typename, level in value[3].items():
            if typename in ['ac', 'mgmt', 'interface', 'ha']:
                logsetstr = logsetstr + \
                    typedic[typename] + ':' + leveldic[level] + '\n'
        table.rows[28].cells[5].text = logsetstr.strip()

    document = Document(docxtempfile)
    table = document.tables[0]  # 处理doc文件中的第一个表格
    dirstr = './' + customname  # 输出目录为客户名称
    for comlist, value in valuelist.items():
        try:
            eval(comlist)(table, value)
        except IndexError:
            print('从返回数据列表取值填入表格时出错，当前处理的数据为：' + str(value))
            continue
        except KeyError:
            print('暂时不能处理该项，当前处理的数据为：' + comlist + '+' + str(value))
            continue
    table.rows[0].cells[2].text = customname
    # 统一居中
    for i in range(0, 3):
        table.rows[i].cells[2].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(11, 19):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(22, 32):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    # 同一设置字体大小为小五
    for row in table.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.size = Pt(9)
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    try:
        os.makedirs(dirstr)
    except FileExistsError:
        pass
    except PackageNotFoundError:
        print('模板未找到，请查看是否存在以下文件：' + docxtempfile)
        exit(-1)
    try:
        document.save(customname + "/" + hostip+ timeModule.strftime('-%m%d%H%M%S', timeModule.localtime()) + ".docx")
    except FileNotFoundError:
        print('未找到输出目录./' + customname + '/')


def fwtos020_outdoc(docxtempfile, valuelist, customname, hostip):

    def model(table, value):
        table.rows[1].cells[2].text = value

    def version(table, value):
        table.rows[1].cells[5].text = value[0]
        table.rows[2].cells[2].text = value[1]

    def time(table, value):
        table.rows[16].cells[5].text = value[0]
        table.rows[2].cells[5].text = value[1]

    def ntp(table, value):
        table.rows[23].cells[5].text = 'ntp服务状态：' + \
            value[1] + '\n配置的同步地址为：' + value[0]

    def uptime(table, value):
        table.rows[15].cells[5].text = str(value) + '天'

    def sysinfo(table, value):
        table.rows[11].cells[5].text = str(value) + '%'

    def meminfo(table, value):
        table.rows[12].cells[5].text = str(value) + '%'

    def interface(table, value):
        rxerrorstr = ''
        txerrorstr = ''
        rxdropstr = ''
        txdropstr = ''
        rxoverrunstr = ''
        txoverrunstr = ''
        rxframe = ''
        txcarrier = ''
        for interface, packets in value.items():
            rxerrorstr = rxerrorstr + interface + ':' + packets[0][1] + '\n'
            txerrorstr = txerrorstr + interface + ':' + packets[1][1] + '\n'
            rxdropstr = rxdropstr + interface + ':' + packets[0][2] + '\n'
            txdropstr = txdropstr + interface + ':' + packets[1][2] + '\n'
            rxoverrunstr = rxoverrunstr + \
                interface + ':' + packets[0][3] + '\n'
            txoverrunstr = txoverrunstr + \
                interface + ':' + packets[1][3] + '\n'
            rxframe = rxframe + interface + ':' + packets[0][4] + '\n'
            txcarrier = txcarrier + interface + ':' + packets[1][4] + '\n'
        # table.rows[18].cells[5].text = rxerrorstr.strip()  # RX错误包数
        # table.rows[19].cells[5].text = txerrorstr.strip()  # TX错误包数
        # table.rows[20].cells[5].text = rxdropstr.strip()  # RX丢弃包数
        # table.rows[21].cells[5].text = txdropstr.strip()  # TX丢弃包数
        # table.rows[22].cells[5].text = rxoverrunstr.strip()  # RX覆盖错误硬件包数
        # table.rows[23].cells[5].text = txoverrunstr.strip()  # TX覆盖错误硬件包数
        # table.rows[24].cells[5].text = rxframe.strip()  # RX帧错误包数
        # table.rows[25].cells[5].text = txcarrier.strip()  # TX帧错误包数
        string = 'RX错误包数：\n' + rxerrorstr + 'RX丢弃包数：\n' + rxdropstr + \
            'TX错误包数：\n' + txerrorstr + 'TX丢弃包数：\n' + txdropstr.strip()
        table.rows[13].cells[5].text = string

    # def neverexp(table, value):
    #     table.rows[26].cells[5].text = '最大长连接数:' + \
    #         value[1] + '\n已建立长连接数:' + value[2]

    def snmp(table, value):
        table.rows[33].cells[5].text = value.strip()

    # def snmpcfg(table, value):
    #     snmpstr = '\nsnmp管理主机配置为:\n'
    #     for host in value:
    #         snmpstr = snmpstr + host[0] + ':' + host[1] + '\n'
    #     table.rows[27].cells[5].text = (
    #         table.rows[27].cells[5].text + snmpstr).strip()

    # def dhcp(table, value):
    #     table.rows[28].cells[5].text = value.strip()

    def mactable(table, value):
        table.rows[17].cells[5].text = 'link:' + \
            str(value[0]) + ',pool:' + str(value[1])

    def policynum(table, value):
        table.rows[18].cells[5].text = str(value)

    def natnum(table, value):
        table.rows[19].cells[5].text = str(value)

    def lognum(table, value):
        table.rows[20].cells[5].text = str(value[0])

    def devname(table, value):
        table.rows[14].cells[5].text = value

    def webui(table, value):
        table.rows[24].cells[5].text = str(value)

    def servicestatus(table, value):
        string = ''
        for service, status in value.items():
            string = string + service + '状态:' + status + '\n'
        table.rows[25].cells[5].text = string.strip()

    def localservice(table, value):
        string = ''
        for service, addresses in value.items():
            addressstr = ''
            #将第一个元素相同的列表合并，即将同一个区域的限制地址合并到一个列表中
            newaddrdict = {}
            for key, value in addresses:
                if key not in newaddrdict.keys():
                    newaddrdict[key] = [key]
                newaddrdict[key].append(value)
            results = list(newaddrdict.values())
            for address in results:
                addressstr = addressstr + '\n限制区域：'+address[0] + ';限制地址：'+','.join(address[1:])
            string = string + service + ':' + addressstr + '\n'
        table.rows[26].cells[5].text = string.strip()


    def ALG(table, value):
        string = ''
        for algiter in value:
            string = string + algiter[1] + '端口' + algiter[0]+'，状态：'+algiter[2] + '\n'
        table.rows[27].cells[5].text = string.strip()

    def session(table, value):
        string = '' + '已建立tcp连接超时时间:' + value[5] + '\n已握手连接超时时间:' + value[3] + \
            '\n已断开连接超时时间:' + value[1] + '\n其他tcp连接超时时间' + value[2] + '\nUDP连接超时时间:' + value[4]
        # table.rows[41].cells[5].text = string.strip()
        table.rows[28].cells[5].text = value[0]

    def area(table, value):
        areastr = ''
        for area in value:
            areastr = areastr + '区域\''+area[0] +'\'包含接口'+area[1]+ ',默认权限为:' + area[2] + '\n'
        table.rows[29].cells[5].text = areastr.strip()

    def ha(table, value):
        setstr = ''
        if value[1] == 'sp':
            table.rows[32].cells[5].text = '当前为连接保护模式'
        elif value[1] == 'as':
            # curstats = value[2]
            # for curstat in curstats:
            #     if curstat[0]==value[3]:
            #     #此为多个列表需要循环对比当前心跳口ip
            #         table.rows[48].cells[5].text = curstat[2]
            #         table.rows[49].cells[5].text = curstat[1]
            #         try:
            #             table.rows[50].cells[5].text = curstat[5]
            #         except IndexError:
            #             table.rows[50].cells[5].text = '监控接口未配置'
            # table.rows[47].cells[5].text = value[0]
            if value[0]=='disable':
                table.rows[32].cells[5].text = '当前运行状态为disable'
            else:
                table.rows[32].cells[5].text = '当前运行状态为' + value[2][0][3]

    def log(table, value):
        typedic = {
            'mgmt': '配置管理',
            'system': '阻断策略',
            'pf': '阻断策略',
            'conn': '连接',
            'ac': '访问控制',
            'secure': '防攻击',
            'dpi': '深度内容检测',
            'stat': '端口流量',
            'asse': '反垃圾邮件',
            'ar': '应用程序识别',
            'user_auth': '用户认证',
        }
        leveldic = {
            '0': '紧急',
            '1': '告警',
            '2': '严重',
            '3': '错误',
            '4': '警示',
            '5': '通知',
            '6': '信息',
            '7': '调试'}
        logsetlist = []
        logsetstr = '已开启以下模块日志：\n'
        for cnfstr in value[2]:
            if cnfstr in typedic.keys():
                logsetlist.append(typedic[cnfstr])
        serverlist = ','.join(value[0])
        logsetstr = logsetstr + ','.join(logsetlist)
        logserverstr = '日志服务器地址:' + \
            serverlist + '\n是否开启外发外发:' + value[1]
        logsetstr = '\n' + logsetstr + ',等级:' + leveldic[value[3]]
        table.rows[30].cells[5].text = logserverstr + logsetstr.strip()

    def authpolicy(table, value):

        table.rows[31].cells[5].text = '密码复杂度：' + value[0] + '\n密码最小长度：' + \
            value[1] + '\n允许登录失败次数：' + value[2] + '\n账号锁定时间(秒)：' + value[3]

    document = Document(docxtempfile)
    table = document.tables[0]  # 处理doc文件中的第一个表格
    dirstr = './' + customname  # 输出目录为客户名称
    for comlist, value in valuelist.items():
        try:
            eval(comlist)(table, value)
        # except IndexError:
        #     print('从返回数据列表取值填入表格时出错，当前处理的数据为：' + str(value))
        #     continue
        except (KeyError,IndexError) :
            print('暂时不能处理该项，当前处理的数据为：' + comlist + '+' + str(value))
            continue

    table.rows[0].cells[2].text = customname
    # 统一居中
    for i in range(0, 3):
        table.rows[i].cells[2].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
        table.rows[i].cells[2].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(11, 21):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    for i in range(23, 34):
        table.rows[i].cells[5].paragraphs[0].paragraph_format.alignment = WD_TABLE_ALIGNMENT.CENTER
    #同一设置字体大小为小五
    for row in table.rows:
        for cell in row.cells:
            paragraphs = cell.paragraphs
            for paragraph in paragraphs:
                for run in paragraph.runs:
                    font = run.font
                    font.size = Pt(9)
    table.alignment = WD_TABLE_ALIGNMENT.CENTER

    try:
        os.makedirs(dirstr)
    except FileExistsError:
        pass
    except PackageNotFoundError:
        print('模板未找到，请查看是否存在以下文件：' + docxtempfile)
        exit(-1)
    try:
        document.save(customname + "/" + hostip+ timeModule.strftime('-%m%d%H%M%S', timeModule.localtime())+ ".docx")
    except FileNotFoundError:
        print('未找到输出目录./' + customname + '/')


if __name__ == '__main__':
    # 测试用
    valuelist = {'model': 'TopSDWAN(SDW-11106-CPELW-S)', 'version': ('', 'Q2104039153'),
     'time': ('2022-04-09 21:15:33', '2022-04-09 21:17:23'), 'ntp': ('', 'stopped'), 'uptime': 0, 'sysinfo': '0.91',
     'meminfo': '19.60', 'interface': {}, 'snmp': 'not running', 'mactable': ('5', '8186'), 'policynum': ' 0',
     'natnum': ' 0', 'lognum': '0', 'devname': 'TopsecOS', 'webui': '180',
     'servicestatus': {'sshd': 'stop', 'telnetd': 'stop', 'httpd': 'start', 'monitord': 'start'},
     'localservice': {'webui': [['area_eth0', 'any'], ['area_wlan', 'any']], 'ssh': [], 'telnet': []}, 'ALG': [],
     'session': ('on', 'default', 'default', 'default', 'default', 'default'),
     'area': [['area_eth0', 'eth0 ', 'on '], ['area_wlan', 'wlan0 wlan1 ', 'on ']], 'ha': ('enable', 'as', [['1.1.1.1', 'enable', '254', 'MASTER', '0']], '1.1.1.1'),
     'log': (['192.168.1.253'], 'disable', ['none'], '0'), 'authpolicy': ('', '', '', '')}

    fwtos020_outdoc('template\\fwtos020.docx', valuelist, 'tos', '1.1.1.1')
