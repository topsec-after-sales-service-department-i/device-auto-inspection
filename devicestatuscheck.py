#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: devicestatuscheck
# @Function:
# @Author: RyneZ
# @Time: 2021/4/13 14:59
import logging
import sys
import paramiko
import time
import re
import xmltodict
import json
import csv
from docx import Document
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.opc.exceptions import *
from numpy import *
from configdeal.__init__ import *
from docxdeal import *
import serial
import argparse
import urllib.request
import urllib.error

class Device():
    def __init__(
                self,
                customname,
                type,
                hostname='192.168.1.254',
                port=22,
                username='superman',
                password='talent'):
            self.hostname = hostname
            self.port = port
            self.username = username
            self.password = password
            self.customname = customname
            self.type = type
            if self.type in ['fwng20', 'fwg22', 'fwng23', 'fwng26']:
                self.encoding = 'UTF-8'
            else:
                self.encoding = 'gb2312'
            self.templist = []  # 但一次没有返回所有内容时临时存放在此
            self.outputStr = b''
            self.outputDic = locals()  # 存放对应检查项返回的原始数据，key为xml中各检查项，如：uptime
            self.checkValues = {}  # 存放对应检查项处理后需要展示的值，key为xml中各检查项

    def outputDOCX(self, type):
        """
        根据设备类型选择模板输出文件。
        :param type:字符串，设备类型，eg:ng23。
        :return:
        """
        try:
            tempname = 'template\\' + type + '.docx'
            eval(
                type +
                '_outdoc')(
                tempname,
                self.checkValues,
                self.customname,
                self.hostname)
        except NameError:
            print('该命令无法输出结果到模板中，请检查日志文件手动调整巡检报告：' + type)
        except PackageNotFoundError:
            print('未找到模板文件，请检查template文件夹下对应巡检模板是否存在...')

class sshDevice(Device):
    def __init__(
            self,
            customname,
            type,
            hostname='192.168.1.254',
            port=22,
            username='superman',
            password='talent'):
        super().__init__(customname,type,hostname,port,username,password)
        self.s = paramiko.SSHClient()
        self.s.load_system_host_keys()
        self.s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.s.connect(
            hostname=self.hostname,
            port=int(self.port),
            username=self.username,
            password=self.password)  # 设备不支持exec_command
        self.channel = self.s.invoke_shell(width=256, height=100)  # 模拟终端


    def sshClient_ready(self, type, execmds):
        """
        1.执行各巡检需要的命令
        2.将返回值处理后放入字典
        :param type: 字符串，设备类型，eg:ng23。用来调用函数、生成文件等
        :param execmds: 字典，包含需要执行的命令，eg:{'sysinfo':'system information',}
        :return:
        """
        # self.outputDic=locals()
        paramiko.util.log_to_file("paramiko.log")
        # print('已建立')
        # stdin,stdout,stderr=s.exec_command('system version',get_pty=True) #测试设备不支持该方式
        # for std in stdout.readlines():
        #     print(std)
        self.channel.recv(65535)  # 去除开头无用信息
        for info, execmd in execmds.items():
            cmdlist = execmd.split(';')
            print('当前执行命令为：' + execmd)
            try:
                # if 0:
                if info == 'sysinfo':
                    values = 0  # 30次求平均值
                    self.outputDic[info] = []
                    for i in range(10):
                        print('第' + str(i + 1) + '次取值')
                        self.ssh_exec(info, execmd)
                        values = values + \
                            float(eval((type + '_' + info))(self.outputDic[info]))
                        time.sleep(1)
                    self.checkValues.update(
                        {info: "{:.2f}".format(values / 10)})  # 此处30需与循环次数相同
                else:
                    self.outputDic[info] = []
                    for cmd in cmdlist:
                        self.ssh_exec(info, cmd)
                    self.checkValues.update(
                        {info: eval((type + '_' + info))(self.outputDic[info])})  # 动态执行处理返回值的函数，容易出错
            except KeyboardInterrupt:
                input('手动中断，按回车退出')
                sys.exit(-1)
            except BaseException:
                if self.outputDic[info]:
                    print(
                        ''.join(
                            ('未能处理当前命令：',
                             self.outputDic[info][0],
                                '程序执行完毕后，该命令的返回值将保存至log/目录下，请参照该内容自行填写报告中缺失内容。')))
                    continue
                else:
                    input('未知错误，按回车继续...')


        self.s.close()
        ##################
        # 如果需要调试新模板或者新设备，可以在此终止程序，目前已获取所有的返回值，保存在self.outputDic中
        #print(self.outputDic)
        # 处理后的检查项返回值保存在self.checkValues中，通过outputDOCX函数输出到文件中
        #print(self.checkValues)
        # exit()
        ##################

    def ssh_exec(self, info, execmd):
        """
        接收一个字符串(待执行命令)，无返回值，将命令及获取到的返回值存放到内部字典变量outputDic中。
        :param info: 字符串,用以生成存放返回值字典的key
        :param execmd: 字符串，待执行命令
        :return:
        """
        self.channel.send(execmd + '\n')
        count = 0
        time.sleep(2)
        while True:
            stdout = self.channel.recv(65535)
            if stdout[-2:] == b'# ':  # 通过判断是否终端为等待输入状态来判断是否获取到所有返回数据
                regStr = re.compile('(.*?\\r\\n)')
                if count > 0:
                    self.templist.extend(
                        re.findall(
                            regStr, stdout.decode(
                                self.encoding)))
                    self.outputDic[info] = self.outputDic[info] + self.templist
                    self.templist = []
                else:
                    self.outputDic[info] = self.outputDic[info] + re.findall(
                        regStr, stdout.decode(self.encoding))
                    self.outputStr = self.outputStr + stdout[:-12]
                # print(stdout.decode())
                break

            elif stdout[-11:] == b'\r\n --More--':  # 判断是否当前模拟终端有未获取到的数据
                self.outputStr = self.outputStr + stdout[:-9]
                regStr = re.compile('(.*?\\r\\n)')
                #print(re.findall(regStr, stdout.decode('utf-8')))
                self.templist.extend(
                    re.findall(
                        regStr, stdout.decode(
                            self.encoding)))
                self.channel.send(' ')
                time.sleep(1)
            else:
                print(stdout)
                input('出现异常，按回车键退出...')
                self.s.close()
                sys.exit(-1)
            count += 1
        # print(self.outputStr)  #将原始输出保存


    # def outputDOCX(self, type):
    #     """
    #     根据设备类型选择模板输出文件。
    #     :param type:字符串，设备类型，eg:ng23。
    #     :return:
    #     """
    #     try:
    #         tempname = 'template\\' + type + '.docx'
    #         eval(
    #             type +
    #             '_outdoc')(
    #             tempname,
    #             self.checkValues,
    #             self.customname,
    #             self.hostname)
    #     except NameError:
    #         print('该命令无法输出结果到模板中，请检查日志文件手动调整巡检报告：' + type)
    #     except PackageNotFoundError:
    #         print('未找到模板文件，请检查template文件夹下对应巡检模板是否存在...')

class serialDevice(Device):
    def __init__(self,
        customname,
        type,
        hostname = 'COM5',
        port = 9600,
        username = 'superman',
        password = 'talent'):
        super().__init__(customname, type, hostname, port, username, password)

        self.s = serial.Serial(hostname, int(port), timeout=0.2)

        if not self.s.isOpen():
            raise TimeoutError
        self.s.write('\n'.encode(self.encoding))
        while True:
            login_info = self.s.readline()
            if login_info:
                print(login_info)
                if login_info.endswith(b'login: '):
                    self.s.write((self.username+'\n').encode(self.encoding))
                if b'Password:' in login_info:
                    self.s.write((self.password+'\n').encode(self.encoding))
                if b'ncorrect' in login_info:
                    print('密码错误...')
                    sys.exit()
                if login_info.endswith(b'# '):
                    print("设备串口登录成功\r\n")
                    break


    def serialClientReady(self, type, execmds):
        for info, execmd in execmds.items():
            cmdlist = execmd.split(';')
            print('当前执行命令为：' + execmd)
            try:
                # if 0:
                if info == 'sysinfo':
                    values = 0  # 30次求平均值
                    self.outputDic[info] = []
                    for i in range(10):
                        print('第' + str(i + 1) + '次取值')
                        self.serialExec(info, execmd)
                        values = values + \
                                 float(eval((type + '_' + info))(self.outputDic[info]))
                        time.sleep(1)
                    self.checkValues.update(
                        {info: "{:.2f}".format(values / 10)})  # 此处30需与循环次数相同
                else:
                    self.outputDic[info] = []
                    for cmd in cmdlist:
                        self.serialExec(info, cmd)
                    self.checkValues.update(
                        {info: eval((type + '_' + info))(self.outputDic[info])})  # 动态执行处理返回值的函数，容易出错
            except KeyboardInterrupt:
                input('手动中断，按回车退出')
                sys.exit(-1)
            except BaseException:
                if self.outputDic[info]:
                    print(
                        ''.join(
                            ('未能处理当前命令：',
                             self.outputDic[info][0],
                             '程序执行完毕后，该命令的返回值将保存至log/目录下，请参照该内容自行填写报告中缺失内容。')))
                    continue
                else:
                    input('未知错误，按回车继续...')
        self.s.close()
        # 如果需要调试新模板或者新设备，可以在此终止程序，目前已获取所有的返回值，保存在self.outputDic中
        #print(self.outputDic)
        # 处理后的检查项返回值保存在self.checkValues中，通过outputDOCX函数输出到文件中
        #print(self.checkValues)
        # exit()
        ##################

    def serialExec(self,info, execmd):

        self.s.write((execmd+'\n').encode(self.encoding))
        count = 0

        while True:
            stdout = self.s.readline()
            #print(stdout)
            if stdout[-2:] == b'# ':  # 通过判断是否终端为等待输入状态来判断是否获取到所有返回数据
                regStr = re.compile('(.*?\\r\\n)')
                if count > 0:
                    self.templist.extend(
                        re.findall(
                            regStr, stdout.decode(
                                self.encoding)))
                    self.outputDic[info] = self.outputDic[info] + self.templist
                    self.templist = []
                else:
                    self.outputDic[info] = self.outputDic[info] + re.findall(
                        regStr, stdout.decode(self.encoding))
                    self.outputStr = self.outputStr + stdout[:-12]
                # print(stdout.decode())
                break

            elif stdout[-9:] == b' --More--':  # 判断是否当前模拟终端有未获取到的数据
                # self.outputStr = self.outputStr + stdout[:-9]
                # regStr = re.compile('(.*?\\r\\n)')
                # # print(re.findall(regStr, stdout.decode('utf-8')))
                # self.templist.extend(
                #     re.findall(
                #         regStr, stdout.decode(
                #             self.encoding)))
                self.s.write(' '.encode(self.encoding))
                time.sleep(1)
            else:
                regStr = re.compile('(.*?\\r\\n)')
                self.templist.extend(
                    re.findall(
                        regStr, stdout.decode(
                            self.encoding)))
                self.outputDic[info] = self.outputDic[info] + self.templist
                self.templist = []
            count += 1

def getCommands(type, execmds):
    return execmds[type]


def main():

    # 获取巡检各设备所需的命令信息

    # 从excel中获取设备信息
    with open('deviceinfo.csv') as f:
        customname = input('请输入客户名称后回车即可开始：')
        devices = csv.reader(f)
        next(devices)
        for deviceinfo in devices:
            # 待添加多线程
            # print(deviceinfo)
            hostname = deviceinfo[0]
            port = deviceinfo[1]
            if hostname.startswith('COM'):
                mode='serial'
            else:
                port = deviceinfo[1]
                mode='ssh'
            username = deviceinfo[2]
            password = deviceinfo[3]
            type = deviceinfo[4]
            # 获取需要执行的命令
            try:
                with open('commands\\' + type + 'commands.xml') as configfile:
                    xmlcontentstr = configfile.read()
                commanddict = xmltodict.parse(xmlcontentstr)
                # print(json.dumps(commanddict, indent=4))
                execmds = json.loads(json.dumps(commanddict))
                execmd = execmds[type]  # 获取当前类型设备所需要的命令
            except BaseException:
                input('未在commands目录下找到对应设备命令列表，请检查文件是否存在或权限配置是否正常。按回车继续...')
                continue
            try:
                print('即将对' + hostname + '进行巡检...')
                if mode=='ssh':
                    device = sshDevice(
                        hostname=hostname,
                        port=str(port),
                        username=username,
                        password=password,
                        customname=customname,
                        type=type)
                    device.sshClient_ready(type, execmd)
                    # print(device.checkValues)
                    # device.outputDOCX(type)
                    # 输出命令返回值到log目录下
                elif mode=='serial':
                    device = serialDevice(
                        hostname=hostname,
                        port=str(port),
                        username=username,
                        password=password,
                        customname=customname,
                        type=type)
                    device.serialClientReady(type,execmd)
            except paramiko.ssh_exception.AuthenticationException:
                print('设备登录认证失败...')
                continue
            except TimeoutError:
                print('连接远程设备失败...')
                continue
            except serial.serialutil.SerialException:
                print('连接串口失败...')
                continue
            except ValueError:
                logging.error('用户名、密码、端口等存在一项躲着多项未正确填写')
                print('请检查用户名、密码、端口等是否正确填写')
                continue
            try:
                with open('log\\' + hostname + time.strftime('-%Y%m%d%H%M%S', time.localtime()) + '.log', 'a+') as logfile:
                    logdic = device.outputDic
                    logdic.pop('self')
                    #logdic.pop('login_info')
                    for key, value in logdic.items():
                        if isinstance(logdic[key], str):
                            continue
                        for i in range(0, len(value)):
                            logdic[key][i] = logdic[key][i].strip()
                        logdic[key] = list(filter(None, logdic[key]))
                    logfile.write(json.dumps(logdic, indent=4))
            except BaseException:
                print('日志文件输出失败')
            except Exception as E:
                print(E)
            try:
                device.outputDOCX(type)
            except BaseException:
                print('巡检报告输出失败，请按照日志文件手动填写。')
        input('处理完成，输出文件保存在以客户名称命名的文件夹中。\n按回车键退出')
        sys.exit(0)
        # device.getConfigValue()
        # print(device.checkValues)

def update(curversion):
    verurl='http://101.43.211.229/mytools/devicecheck/latestver.html'
    fileurl = 'https://gitlab.com/topsec-after-sales-service-department-i/autoinspection_public/-/archive/main/autoinspection_public-main.zip'
    print('开始更新...请稍候')
    try:
        print('获取版本信息...请稍候')
        fp = urllib.request.urlopen(verurl)
        lastestver=fp.read().decode()
        fp.close()
        if curversion==lastestver:
            input('当前版本已是最新版本...按任意键退出')
            sys.exit(1)
        print('最新版本为'+lastestver+'，开始下载最新版本...')
        f = urllib.request.urlopen(fileurl)
        with open('防火墙巡检工具v'+lastestver+'.zip', "wb") as code:
            code.write(f.read())
        print('下载完成，解压目录下防火墙巡检工具v'+lastestver+'.zip覆盖原文件即可。')
    except urllib.error.HTTPError:
        print('下载地址出错，请及时反馈...')
    except urllib.error.URLError:
        print('请检查DNS或者网络连接情况...')
    except PermissionError:
        print('写入文件失败，请检查目录权限...')


if __name__ == "__main__":
    curversion='0.2.4'

    parser = argparse.ArgumentParser(
        description='默认直接执行无需附带参数\n更新程序执行： syncftpfile -update')
    parser.add_argument(
        '-update',
        action='store_true',
        help='检查更新并下载最新版本')
    isupdte = parser.parse_args().update

    if isupdte:
        update(curversion)
    else:
        print('当前版本：'+curversion+'\n')
        main()
